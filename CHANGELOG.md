##2.1.0 (Oct 17 2017)

* Improve UI

* Fix minor bugs

* Fix MassAction

##2.0.0 (Jul 21 2016)

Initial Release

Feature

* Admin can add pages, category, custom link , custom text from left sidebar into the mega menu

* There are two mode for a level 1 menu : mega menu and fly out menu.

* Admin are able to add sub-categories and /or new arrivals,featured products to the category level 1 menu item

* Admin are able to use move up/down/under, out of icon to arrange the menu item

* Admin can drag and drop the menu item to re-arrange menu
