<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 04/05/2016
 * Time: 22:06
 */
namespace Magenest\MegaMenu\Controller\Menu;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Load extends \Magento\Framework\App\Action\Action
{

    protected $_menuFactory;

    protected $_menuItemFactory;


    /**
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magenest\MegaMenu\Model\MenuFactory $menuFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magenest\MegaMenu\Model\MegaMenuFactory $menuFactory,
        \Magenest\MegaMenu\Model\MenuEntityFactory $menuEntityFactory
    ) {
        parent::__construct($context);
        $this->_menuFactory = $menuFactory;
        $this->_menuItemFactory = $menuEntityFactory;
    }//end __construct()


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */



    public function execute()
    {

        $itemData = array('name'=> 'Google.com', 'link'=>'http://google.com');
        
        $menuEntity = $this->_menuItemFactory->create();
        $menuEntity->setData($itemData)->save();
    }//end execute()


    /**
     * @param $topLevelMenus
     * @return string
     */
    public function getMenuHtml($topLevelMenus, $params)
    {
        $isMegaContent = false;
        $currentLevel  = $topLevelMenus->getFirstItem()->getLevel();

        $liClass = '';
        if ($currentLevel == 0) {
            $class   = $params['ulClassLevel0'];
            $html    = "<ul class='$class'>";
            $liClass = 'item-parent';
        } else {
            $html = '<ul>';
        }

        if ($topLevelMenus->getSize() > 0) {
            /*
                @var  $theMenu  \Magenest\MegaMenu\Model\Menu
            */
            foreach ($topLevelMenus as $theMenu) {
                $link   = $theMenu->getLink();
                $title  = $theMenu->getTitle();
                $level  = $theMenu->getLevel();
                $isMega = $theMenu->getData('is_mega');

                if ($currentLevel == 1 && $isMega == '1') {
                    $isMegaContent = true;

                    /*
                        <div class="drop-down large">
                        <div class="mega-menu">
                        <div class="row">
                    <div class="col-md-4">*/
                    $html    .= '<li>';
                    $html    .= '<div class="drop-down large">';
                    $html    .= '<div class="mega-menu">';
                    $html    .= '<div class="row">';
                    $row_wide = 'col-md-4';
                    $html    .= " <div class='$row_wide'>";

                    $html .= " <div class='some-context'>{$title}</div>";

                    $html .= ' </div>';
                    $html .= ' </div>';
                    $html .= ' </div>';
                    $html .= ' </div>';
                    $html .= '</li>';
                } //end if
                //
                else {
                    $html .= "<li data-level='{$level}' class='$liClass'>"."<a href ='{$link}' >{$title} </a>";

                    if ($theMenu->hasChildren()) {
                        $children = $theMenu->getF1Children();
                        $html    .= $this->getMenuHtml($children, $params);
                    }

                    $html .= '</li>';
                }
            }//end foreach
        }//end if

        if (!$isMegaContent) {
            $html .= '</ul>';
        } else {
            $html .= '</ul>';
        }

        return $html;


    }//end get_menu_html()
}//end class
