<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 24/05/2017
 * Time: 17:12
 */

namespace Magenest\MegaMenu\Controller\Menu;

use Magento\Framework\App\ResponseInterface;

class Test extends \Magento\Framework\App\Action\Action
{

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $input ='0,2,5';

       /** @var \Magenest\MegaMenu\Model\MenuEntity $menu */
        $menu =$this->_objectManager->create('Magenest\MegaMenu\Model\MenuEntity')->load(71);
        $treeInfo = $menu->getChildrenTreeFormat();
        var_dump($treeInfo);

    }
}
