<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 26/04/2016
 * Time: 10:36
 */
namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magento\Framework\Controller\ResultFactory;

class CategoryAjax extends \Magento\Backend\App\Action
{

    protected $_categoryFactory;

    protected $_categoryRepository;

    protected $resultJsonFactory;
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;


    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry     = $registry;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_categoryFactory    = $categoryFactory;
        $this->_categoryRepository = $categoryRepository;

        $this->_storeManager = $storeManager;

        parent::__construct($context);

    }//end __construct()


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $rootCat = $this->_storeManager->getStore()->getRootCategoryId();

        $categoryModel      = $this->_categoryFactory->create();
        $categoryCollection = $categoryModel->getCategories($rootCat, 4, false, true, false);
        $categoryCollection->addNameToResult()->load();
        $catData = array();

        if ($categoryCollection->getSize() > 0) {
            foreach ($categoryCollection as $category) {
                $cateData          = $category->getData();
                $cateData['title'] = $cateData['name'];
                $catData[]         = $cateData;
            }
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        /** @var  $jsonFactory /Magento\Framework\Controller\Result\JsonFactory*/

        $jsonFactory = $objectManager->create('Magento\Framework\Controller\Result\JsonFactory');

        $result = $jsonFactory->create()->setData($catData);

        return $result;

    }//end execute()
}//end class
