<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 09/03/2016
 * Time: 16:20
 */
namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;

class Index extends Action
{


    public function execute()
    {
        $this->_view->getPage()->getConfig()->getTitle()->prepend(
            __('New Menu')
        );
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->getConfig()->getTitle()->prepend(__('MegaMenu Configuration'));
//        $resultPage->getConfig()->getTitle()->prepend(__('Edit'));
        return $resultPage;

    }//end execute()
}//end class
