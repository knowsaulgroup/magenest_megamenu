<?php

namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magenest\MegaMenu\Controller\Adminhtml\Menu;
use Magenest\MegaMenu\Model\MegaMenuFactory;
use Magenest\MegaMenu\Model\MenuEntityFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 * @package Magenest\MegaMenu\Controller\Adminhtml\Menu
 */
class MassDelete extends Menu
{
    protected $_filter;

    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        MegaMenuFactory $megaMenuFactory,
        MenuEntityFactory $menuEntityFactory,
        Registry $registry,
        Filter $filter
    ) {
        $this->_filter = $filter;
        parent::__construct($context, $pageFactory, $megaMenuFactory, $menuEntityFactory, $registry);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_megaMenuFactory->create()->getCollection());
        $deletedGallery = 0;
        $model = $this->_objectManager->create('Magenest\MegaMenu\Model\MegaMenu');
        $menuCollection = $this->_objectManager->create('Magenest\MegaMenu\Model\ResourceModel\MenuEntity\CollectionFactory');
        /** @var \Magenest\MegaMenu\Model\MegaMenu $item */
        if ($collection) {
            foreach ($collection->getItems() as $item) {
                if ($model->load($item['menu_id'])) {
                    /** @var @var \Magenest\MegaMenu\Model\ResourceModel\MenuEntity\Collection $menuCollection */
                    $menuEntities = $menuCollection->create()
                        ->addAttributeToSelect(array('menu_id','entity_id'))
                        ->addAttributeToFilter('menu_id', array('eq' => $item['menu_id']))
                        ->load();
                    foreach ($menuEntities as $menuEntity) {
                        $menuEntity->delete();
                    }

                    $model->load($item['menu_id'])->delete();
                }

                $deletedGallery++;
            }
        }

        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been deleted.', $deletedGallery)
        );
        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('menu/*/');
    }
}
