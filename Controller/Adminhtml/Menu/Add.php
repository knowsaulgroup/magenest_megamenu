<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 09/03/2016
 * Time: 16:20
 */
namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Add extends \Magento\Backend\App\Action
{


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $this->_view->getPage()->getConfig()->getTitle()->prepend(
            __('New Menu')
        );
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->getConfig()->getTitle()->prepend(__('Menu'));
        $resultPage->getConfig()->getTitle()->prepend(__('Edit'));
        return $resultPage;

    }//end execute()
}//end class
