<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 26/04/2016
 * Time: 10:28
 */
namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class CmsAjax extends \Magento\Backend\App\Action
{

    protected $_pageRepository;

    protected $_pageFactory;


    /**
     * @param \Magento\Backend\App\Action\Context        $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Cms\Model\PageRepository          $pageRepository
     * @param \Magento\Framework\Registry                $registry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Cms\Model\PageRepository $pageRepository,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry     = $registry;
        $this->_pageRepository   = $pageRepository;

        $this->_pageFactory = $pageFactory;
        parent::__construct($context);

    }//end __construct()


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $pages = $this->_pageFactory->create()->getCollection()->getData();
        /*
            @var \Magento\Framework\Controller\Result\Json $resultJson
        */
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        /** @var  $jsonFactory /Magento\Framework\Controller\Result\JsonFactory*/

        $jsonFactory = $objectManager->create('Magento\Framework\Controller\Result\JsonFactory');

        $result = $jsonFactory->create()->setData($pages);

        return $result;
    }//end execute()
}//end class
