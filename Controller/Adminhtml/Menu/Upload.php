<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 06/08/2016
 * Time: 10:18
 */

namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResponseInterface;

class Upload extends \Magento\Backend\App\Action
{
    /**
     * @param \Magento\Backend\App\Action\Context             $context
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;

    }//end __construct()


    /**
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_MegaMenu::menu');

    }//end _isAllowed()

    /**
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        try {
            /*
                * @var  $uploader \Magento\MediaStorage\Model\File\Uploader
            */
            $uploader = $this->_objectManager->create(
                'Magento\MediaStorage\Model\File\Uploader',
                array('fileId' => 'icon')
            );
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            /*
                * @var \Magento\Framework\Image\Adapter\AdapterInterface $imageAdapter
            */
            $imageAdapter = $this->_objectManager->get('Magento\Framework\Image\AdapterFactory')->create();
            // $uploader->addValidateCallback('catalog_product_image', $imageAdapter, 'validateUploadFile');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            /*
                * @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory
            */
            $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')->getDirectoryRead(DirectoryList::MEDIA);
            $config         = $this->_objectManager->get('Magento\Catalog\Model\Product\Media\Config');
            $result         = $uploader->save($mediaDirectory->getAbsolutePath($config->getBaseTmpMediaPath()));

            $this->_eventManager->dispatch(
                'mega_menu_attach_upload_after',
                array(
                    'result' => $result,
                    'action' => $this,
                )
            );
            $anaName     = explode('/', $result['file']);
            $desireIndex = (count($anaName) - 1);

            $result['label'] = $anaName[$desireIndex];
            unset($result['tmp_name']);
            unset($result['path']);

            $result['url']  = $this->_objectManager->get('Magento\Catalog\Model\Product\Media\Config')->getTmpMediaUrl($result['file']);
            $result['file'] = $result['file'].'.tmp';
        } catch (\Exception $e) {
            $result = array(
                'error'     => $e->getMessage(),
                'errorcode' => $e->getCode(),
            );
        }//end try

        /*
            * @var \Magento\Framework\Controller\Result\Raw $response
        */
        $response = $this->resultRawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($result));
        return $response;

    }//end execute()
}
