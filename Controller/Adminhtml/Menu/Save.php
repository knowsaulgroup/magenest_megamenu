<?php
namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Url\Validator as UrlValidator;

class Save extends Action
{
    /**
     * @var \Magenest\MegaMenu\Model\Menu
     */
    protected $_model;

    protected $_menuFactory;

    protected $_menuItemFactory;



    /**
     * @param Action\Context $context
     */
    public function __construct(
        Action\Context $context,
        \Magenest\MegaMenu\Model\MegaMenuFactory $menuFactory,
        \Magenest\MegaMenu\Model\MenuEntityFactory $menuEntityFactory
    ) {
        parent::__construct($context);
        $this->_menuFactory = $menuFactory;
        $this->_menuItemFactory = $menuEntityFactory;
    }

    public function saveRelation($id, $children)
    {
        $this->_menuItemFactory->create()->load($id)->addData('children', $children)->save();
    }

    /**
     * @param $structure
     */
    public function processStructure($structure)
    {
        $structureArr = \Zend_Json::decode($structure);

//[{"id":1},{"id":2},{"id":3,"children":[{"id":4},{"id":5}]}]
        $level1 = array();

        if (!empty($structureArr)) {
            foreach ($structureArr as $node) {
                $nodeId = $node['id'];

                $children =array();
                if (isset($node['children'])) {
                    foreach ($node['children'] as $children) {
                        $children[] =  $children['id'];
                    }

                    $this->saveRelation($nodeId, serialize($children));

                    foreach ($node['children'] as $children) {
                        $this->processStructure($children);
                    }
                }
            }
        }
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        $params = $this->getRequest()->getParams();

        $deleteItemsStr = $this->getRequest()->getParam('remove_items');

        if ($deleteItemsStr) {
            $deleteItems = explode(',', $deleteItemsStr);
            if (is_array($deleteItems) && !empty($deleteItems)) {
                foreach ($deleteItems as $menuId) {
                    $menu = $this->_menuItemFactory->create()->load($menuId);

                    if ($menu->getId()) {
                        $menu->delete();
                    }
                }
            }
        }

        /*
            save a menu with information of id, title, level, order ,parentId
        */
        $megaMenuData = array(
            'menu_id' => 0,
            'menu_name' =>'',
//            'menu_event' =>'',
            'menu_template' => 'horizontal_menu',
            'is_active' => 1,
            'store_id' =>0
        );

        $megaMenuData['menu_id'] = (isset($params['menu_id']))?$params['menu_id']:null;
        $megaMenuData['menu_name'] = $params['menu_name'];
//        $megaMenuData['menu_event'] = $params['menu_event'];
        $megaMenuData['menu_template'] = $params['menu_template'];
        $megaMenuData['is_active'] = $params['is_active'];
        $megaMenuData['store_id'] = serialize($params['store_id']);

        $megaMenu = $this->saveMenu($megaMenuData);
        unset($params['menu_id']);
        unset($params['menu_name']);
//        unset($params['menu_event']);
        unset($params['is_active']);
        unset($params['store_id']);
        $menu_id = $megaMenu->getId();

        $newIds = array();

        $updateParents = array();

        $mappedIds = array();

        $savedMenuItems = array();

        if (!isset($params['menu'])) {
            return $resultRedirect->setPath('*/*/');
        };

        $menuParams = $params['menu'];
        $menuParams['menu_id'] = $menu_id;

        $editMode = false;

        foreach ($menuParams as $rawData) {
            //calibrate its children
            if (isset($rawData['children'])) {
                if ($rawData['children'] !='0') {
                    $rawData['children'] = substr($rawData['children'], 2);
                } else {
                    $rawData['children'] = '';
                }
            }


            $menuData   = array(
                'id'            => 0,
                'entity_id'     => 0,
                'menu_id'       => 0,
                'title'         => '',
                'level'         => 0,
                'sort'          => 0,
                'parentId'      => 0,
                'type'          => '',
                'is_mega'       => '',
                'is_top'        => '',
                'parent_id'     => '',
                'link'          => '',
                'obj_id'        => '',
                'brother_name'  => '',
                'icon'          => '',
                'id_temp'       => '',
                'label_color'   => '',
                'label_name'    => '',
                'megaColumn'    => 6,
                'include_child' => '',
                'show_product'  => '',
                'children'       =>'',
                'children_temp'  =>'',
                //new
                'cssClass'  =>'',
                'showIcon'  =>'',
                'leftEnable'  =>'',
                'leftClass'  =>'',
                'leftWidth'  =>'',
                'leftContentHtml'  =>'',
                'mainEnable'  =>0,
                'mainProductSku'  =>'',
                'mainInCategory'  =>'',
                'mainContentType'  =>'',
                'mainColumn'  =>'',
                'mainContentHtml'  =>'',
                'rightEnable'  =>'',
                'rightClass'  =>'',
                'rightWidth'  =>'',
                'rightContentHtml' => '',
                'textColor'  =>'',
                'hoverTextColor'  =>''

            );

            if (isset($rawData['is_new']) && $rawData['is_new'] == 'false') {
                $editMode = true;
            } else {
                $editMode = false;
            }

            if (isset($rawData['leveltemp'])) {
                $menuData['level'] = $rawData['leveltemp'];
            }

            foreach ($menuData as $key => $value) {
                if (isset($rawData[$key])) {
                    $menuData[$key] = $rawData[$key];
                }
            }

            $menuData['entity_id'] = $menuData['id'];
            $menuData['children_temp'] = $menuData['children'];

            if (isset($rawData['id'])) {
                $menuData['id_temp']   = $rawData['id'];
            }

            if (!$editMode) {
                unset($menuData['id']);
                unset($menuData['entity_id']);
            }


            $menuData['menu_id'] = $menu_id;

            $menu = $this->saveMenuItem($menuData);

            if ($menu == null) {
                continue;
            }

            $mappedIds[$rawData['id']] = $menu->getId();
            $savedMenuItems[] = $menu;

            $parentId = $menu->getData('parent_id');

            $parentMenu = $this->_menuItemFactory->create()->getCollection()->addFieldToFilter('entity_id', $parentId);

            if ($parentMenu->getSize() == 0) {
                $updateParents[] = $menu->getId();
            }

            if (!$editMode) {
                $newIds[] = $menu->getId();
            }
        }//end foreach

//        // update the real parent id
//        if (!empty($updateParents)) {
//            foreach ($updateParents as $id) {
//                $menu = $this->_menuFactory->create()->load($id);
//
//                $parentId      = $menu->getData('parent_id');
//                $parentMenuCol = $this->_menuFactory->create()->getCollection()->addFieldToFilter('id_temp', $parentId);
//                if ($parentMenuCol->getSize() > 0) {
//                    $parentMenu = $parentMenuCol->getFirstItem();
//
//                    $realParentId = $parentMenu->getId();
//                    $menu->addData(['parent_id' => $realParentId])->save();
//                }
//            }
//        }


        $this->updateChildren($mappedIds, $savedMenuItems);

        return $resultRedirect->setPath('*/*/');

    }//end execute()

    /**
     * 1 2,3,4
     *10 11,23,44
     *  ['1'=> 10, '2=> 11, 23 => 13]
     * mappedId;
     * @param $mappedIds
     * @param $menus
     */
    public function updateChildren($mappedIds, $menus)
    {
        foreach ($menus as $menuItem) {
            $real=array();
            $children = explode(',', $menuItem->getData('children_temp'));

            if ($children) {
                foreach ($children as $id) {
                    if (isset($mappedIds[$id])) {
                        $real[] = $mappedIds[$id];
                    }
                }
            }

            $realChildStr = implode(',', $real);


            $menuItem->addData(array('children'=>$realChildStr))->save();
        }
    }




    /**
     * {@inheritdoc}
     */
//    protected function _isAllowed()
//    {
//        return $this->_authorization->isAllowed('Magenest_MegaMenu::menu_save');
//    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute1()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Magenest\MegaMenu\Model\Menu $model */
            $model = $this->_model;

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('MegaMenu saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', array('id' => $model->getId(), '_current' => true));
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the MegaMenu'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', array('menu_id' => $this->getRequest()->getParam('id')));
        }

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param $postData
     * @return mixed
     */
    private function saveMenu($postData)
    {
        $menuId = (int)$postData['menu_id'];

        if ($menuId) {
            $menuModel = $this->_menuFactory->create()->load($menuId);
            $menuModel->addData($postData)->save();
        } else {
            unset($postData['menu_id']);
            $menuModel = $this->_menuFactory->create();
            $menuModel->setData($postData)->save();
        }

        return $menuModel;
    }

    private function saveMenuItem($menuData)
    {

        $valid = $this->validateMenuDateItem($menuData);
        if ($valid['result']) {
            return $this->_menuItemFactory->create()->setData($menuData)->save();
        } else {
            //throw new InputException(__($valid['message']));
        }
    }

    /**
     * validate the input
     * @param $menuData
     * @return array
     */
    private function validateMenuDateItem($menuData)
    {
        $isValid = true;
        $message = '';

        $valid = new \Zend_Validate_NotEmpty();
        $isValid = $valid->isValid($menuData['title']);

        $message .= 'title of menu item cant be empty';

     /*   if (isset($menuData['link'])) {
            $validator = new \Zend\Validator\Uri();
            $isValid = $validator->isValid($menuData['link']);
            $message .= 'Url of menu item is mal-formed';

        }*/

        return array(
            'result' => $isValid,
            'message' => $message
        );
    }


    private function deleteMenuItem()
    {

    }
}
