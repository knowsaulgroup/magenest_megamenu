<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 25/03/2016
 * Time: 20:38
 */

namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magento\Framework\App\ResponseInterface;

class Load extends \Magento\Backend\App\Action
{


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        try {
            $path   = $this->storage->getCurrentPath();
            $result = $this->_getStorage()->uploadFile($path);
        } catch (\Exception $e) {
            $result = array(
                       'error'     => $e->getMessage(),
                       'errorcode' => $e->getCode(),
                      );
        }

        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );

    }//end execute()
}//end class
