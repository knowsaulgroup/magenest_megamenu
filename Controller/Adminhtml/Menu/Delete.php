<?php
/**
 * Created by PhpStorm.
 * User: thien
 * Date: 25/08/2017
 * Time: 23:02
 */

namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magenest\MegaMenu\Model\ResourceModel\MegaMenu;
use Magenest\MegaMenu\Model\ResourceModel\MenuEntity;

class Delete extends \Magento\Backend\App\Action
{

    /**
     * Delete Question
     *
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id && (int) $id > 0) {
            try {
                // init model and delete
                $model = $this->_objectManager->create('Magenest\MegaMenu\Model\MegaMenu');
                $menuCollection = $this->_objectManager->create('Magenest\MegaMenu\Model\ResourceModel\MenuEntity\CollectionFactory');
                if ($model->load($id)) {
                    $menuEntities = $menuCollection->create()
                        ->addAttributeToSelect(array('menu_id','entity_id'))
                        ->addAttributeToFilter('menu_id', array('eq' => $id))
                        ->load();
                    foreach ($menuEntities as $menuEntity) {
                        $menuEntity->delete();
                    }

                    $model->delete();
                    $this->messageManager->addSuccessMessage(__(' MegaMenu has been deleted.'));
                    return $resultRedirect->setPath('*/*/');
                }
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/', array('id' => $id));
            }
        }

        // display error message
        $this->messageManager->addErrorMessage(__('MegaMenu to delete was not found.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
