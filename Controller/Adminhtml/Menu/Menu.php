<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 26/04/2016
 * Time: 10:28
 */
namespace Magenest\MegaMenu\Controller\Adminhtml\Menu;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Menu extends \Magento\Backend\App\Action
{

    protected $_pageRepository;

    protected $_pageFactory;

    protected $_menuFactory;


    /**
     * @param \Magento\Backend\App\Action\Context        $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Cms\Model\PageRepository          $pageRepository
     * @param \Magento\Framework\Registry                $registry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Cms\Model\PageRepository $pageRepository,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magenest\MegaMenu\Model\MenuFactory $menuFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry     = $registry;
        $this->_pageRepository   = $pageRepository;

        $this->_pageFactory = $pageFactory;

        $this->_menuFactory = $menuFactory;
        parent::__construct($context);

    }//end __construct()


    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */


    public function getMenus()
    {
        $model = $this->_menuFactory->create();

        $topLevelMenus = $model->getTopLevelMenu();
        $out           = $model->getMenuArray($topLevelMenus);
//        var_dump("jiji");die();
        // $menus = $this->flatten($out);
        $arfFormat = array();
        foreach ($out as $key => $value) {
            $menu       = $this->_menuFactory->create()->load($value);
            $parentId   = $menu->getData('parent_id');
            $hasParent  = false;
            $parentName = '';
            if ($parentId) {
                 $parentMenu = $this->_menuFactory->create()->load($parentId);
                 $parentName = $parentMenu->getTitle();
            }

            if ($parentName) {
                $hasParent = true;
            }

            $brother_name = $menu->getData('brother_name');

            if ($brother_name) {
                $hasBrother = true;
            } else {
                $hasBrother = false;
            }

            $arfFormat[] = array(
                            'entity_id'     => $menu->getId(),
                            'title'         => $menu->getTitle(),
                            'level'         => $menu->getLevel(),
                            'sort'          => $menu->getSort(),
//                            'image'         => $menu->getIcon(),
                            'parent'        => $parentId,
                            'hasParent'     => $hasParent,
                            'parentName'    => $parentName,
                            'hasBrother'    => $hasBrother,
                            'brother_name'  => $brother_name,
//                            'label_name'    => $menu->getData('label_name'),
//                            'label_color'   => $menu->getData('label_color'),
                            'is_mega'       => $menu->getData('is_mega'),
                            'megaColumn'    => $menu->getData('megaColumn'),
                            'type'          => $menu->getData('type'),
//                            'obj_id'        => $menu->getData('obj_id'),
                            'link'          => $menu->getData('link'),
//                            'include_child' => $menu->getData('include_child'),
//                            'show_product'  => $menu->getData('show_product'),
                            'is_new'        => false,

                           );
        }//end foreach

        return $arfFormat;

        // $output = [];
    }//end getMenus()


    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        /** @var  $jsonFactory /Magento\Framework\Controller\Result\JsonFactory*/

        $jsonFactory = $objectManager->create('Magento\Framework\Controller\Result\JsonFactory');
        
        $result = $jsonFactory->create()->setData(1);

        return $result;

    }//end execute()
}//end class
