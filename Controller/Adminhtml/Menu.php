<?php
namespace Magenest\MegaMenu\Controller\Adminhtml;

use Magenest\MegaMenu\Model\MegaMenuFactory;
use Magenest\MegaMenu\Model\MenuEntityFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Menu
 * @package Magenest\MegaMenu\Controller\Adminhtml
 */
abstract class Menu extends \Magento\Backend\App\Action
{
    protected $_megaMenuFactory;

    protected $_menuEntityFactory;

    protected $_pageFactory;

    protected $_coreRegistry;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        PageFactory $pageFactory,
        MegaMenuFactory $megaMenuFactory,
        MenuEntityFactory $menuEntityFactory,
        Registry $registry
    ) {
        $this->_megaMenuFactory = $megaMenuFactory;
        $this->_menuEntityFactory = $menuEntityFactory;
        $this->_coreRegistry = $registry;
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_pageFactory->create();
        $resultPage->setActiveMenu('Magenest_MegaMenu::menu')
            ->addBreadcrumb(__('MegaMenu Gallery Date'), __('MegaMenu Gallery Date'));

        $resultPage->getConfig()->getTitle()->set(__('MegaMenu Gallery Date'));
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_MegaMenu::menu');
    }
}
