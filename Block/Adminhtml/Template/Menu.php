<?php
namespace Magenest\MegaMenu\Block\Adminhtml\Template;

use Magento\Framework\Data\Form;
use Magento\Framework\Data\Form\AbstractForm;
use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;
use Magento\Framework\Escaper;

class Menu extends \Magento\Framework\Data\Form\Element\AbstractElement
{
    protected $_elements;

   /** @var \Magento\Framework\Registry  */
    protected $_coreRegistry;

    /**
     * @param Factory $factoryElement
     * @param CollectionFactory $factoryCollection
     * @param Escaper $escaper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Data\Form\Element\Factory $factoryElement,
        \Magento\Framework\Data\Form\Element\CollectionFactory $factoryCollection,
        Escaper $escaper,
        \Magento\Framework\Registry $registry,
        $data = array()
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($factoryElement, $factoryCollection, $escaper, $data);
    }
    public function getElementHtml()
    {
        /*
         *
         */

        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $template = $objectManager->create('Magento\Backend\Block\Template');
        /*
         * @var $lay \Magento\Framework\View\LayoutInterface
         */
        $lay = $objectManager->create('Magento\Framework\View\LayoutInterface');
        $xxx = $lay->createBlock('Magento\Framework\View\Element\Template')
            ->setTemplate('Magenest_MegaMenu::menu/themain.phtml')
            ->toHtml();
        $html = "";
//        var_dump($xxx);

        return $xxx;
    }

    public function getMenuId()
    {
        $this->_coreRegistry->registry('menu_id');
    }
}
