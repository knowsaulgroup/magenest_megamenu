<?php
namespace Magenest\MegaMenu\Block\Adminhtml\Menu\Edit;

/**
 * Class Form
 * @package Magenest\YoutubeIntegration\Block\Adminhtml\Gallery\Edit
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

//    protected $_template = 'Magento_Backend::widget/form.phtml';
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            array('data' =>
                array(
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post'
                )
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
