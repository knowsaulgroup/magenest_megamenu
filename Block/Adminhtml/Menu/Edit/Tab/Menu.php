<?php
namespace Magenest\MegaMenu\Block\Adminhtml\Menu\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class Menu extends Generic implements TabInterface
{


    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\ProductVideo\Helper\Media $mediaHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        array $data = array()
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->mediaHelper = $mediaHelper;
        $this->urlBuilder = $context->getUrlBuilder();
        $this->jsonEncoder = $jsonEncoder;
        $this->setUseContainer(true);
    }

    /**
     * Form preparation
     *
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $megaMenu = $this->_coreRegistry->registry("magenest_mega_menu");
//        $galleryGroup = $this->_coreRegistry->registry("youtube_integration_gallery_group");
//        $galleryList = $this->_coreRegistry->registry("youtube_integration_gallery_group_list");
//        $galleryGrValue = $galleryGroup->getData();
//        var_dump($gallery->getData());die();
        $form = $this->_formFactory->create();
        $form->addField('magenest_menu-entity', 'note', array());
        $fieldset = $form->addFieldset('new_magenest_menu-entity', array());


        if ($megaMenu->getId()!=null || $megaMenu->getId()!="") {
            $fieldset->addField(
                'entity_id',
                'hidden',
                array('name' => 'id')
            );
        }

//        $fieldset->addField(
//            'gallery_name',
//            'text',
//            [
//                'name' => 'gallery_name',
//                'label' => __('Gallery Name'),
//                'title' => __('Gallery Name'),
//                'required' => true
//            ]
//        );
//        $fieldset->addField(
//            'is_active',
//            'select',
//            [
//                'label' => __('Status'),
//                'title' => __('Status'),
//                'name' => 'is_active',
//                'required' => true,
//                'options' => ['1' => __('Active'), '0' => __('Disable')]
//            ]
//        );
//        $fieldset->addField(
//            'gallery_channel_url',
//            'text',
//            [
//                'class' => 'edited-data',
//                'label' => __('Url'),
//                'title' => __('Url'),
//                'required' => true,
//                'name' => 'gallery_channel_url',
//                'value' => 'https://www.youtube.com/channel/UCK6S9V2nriNNL17czCeo-iQ'
//            ]
//        );
        $fieldset->addType('menu_field', '\Magenest\MegaMenu\Block\Adminhtml\Template\Menu');
        $fieldset->addField(
            'menu_entity',
            'menu_field',
            array(
                'name' => 'menu_entity',
                'class' => 'menu_entity'
            )
        );

        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Retrieve currently viewed product object
     *
     * @return \Magento\Catalog\Model\Product
     */
    protected function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->_coreRegistry->registry('product'));
        }

        return $this->getData('product');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Menu Setting');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Menu Setting');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
