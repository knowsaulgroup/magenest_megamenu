<?php
namespace Magenest\MegaMenu\Block\Adminhtml\Menu\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class General extends Generic implements TabInterface
{
    protected $wysiwyg;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\ProductVideo\Helper\Media $mediaHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwyg,
        array $data = array()
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->mediaHelper = $mediaHelper;
        $this->urlBuilder = $context->getUrlBuilder();
        $this->jsonEncoder = $jsonEncoder;
        $this->wysiwyg = $wysiwyg;
        $this->setUseContainer(true);
    }
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $megaMenu = $this->_coreRegistry->registry("magenest_mega_menu");
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $stores = $objectManager->create('Magento\Store\Model\System\Store');
        $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection');
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('mega_menu_form_fieldset', array());


        if ($megaMenu->getId()!=null || $megaMenu->getId()!="") {
            $fieldset->addField(
                'menu_id',
                'hidden',
                array('name' => 'menu_id')
            );
        }

        $fieldset->addField(
            'menu_name',
            'text',
            array(
                'name' => 'menu_name',
                'label' => __('Menu Name'),
                'title' => __('Gallery Name'),
                'required' => true
            )
        );
        $fieldset->addField(
            'is_active',
            'select',
            array(
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'is_active',
                'required' => true,
                'options' => array('1' => __('Enable'), '0' => __('Disable'))
            )
        );
//        $fieldset->addField(
//            'menu_event',
//            'select',
//            [
//                'label' => __('Menu Event'),
//                'title' => __('Menu Event'),
//                'name' => 'menu_event',
//                'required' => true,
//                'options' => ['hover' => __('Hover'), 'click' => __('Click')]
//            ]
//        );
        $fieldset->addField(
            'menu_template',
            'select',
            array(
                'label' => __('Menu Template'),
                'title' => __('Menu Template'),
                'name' => 'menu_template',
                'required' => true,
                'options' => array('horizontal' => __('Horizontal Menu'), 'vertical_left' => __('Vertical Left Menu'))
            )
        );
        $fieldset->addField(
            'store_id',
            'multiselect',
            array(
                'label' => __('Store Id'),
                'title' => __('Store Id'),
                'required' => true,
                'name' => 'store_id[]',
            //                'value' => '1',
                'values' => $stores->getStoreValuesForForm(false, true),
            )
        );
        $megaValue = $megaMenu->getData();

        if ($megaValue != null) {
            $form->setValues($megaValue);
        }

        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getStoreIds()
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $stores = $objectManager->create('Magento\Store\Model\StoreRepository')->getList();
        $websiteIds = array();
        $storeList = array();
        foreach ($stores as $store) {
            $websiteId = $store["website_id"];
            $storeId = $store["store_id"];
            $storeName = $store["name"];
            $storeList[$storeId] = $storeName;
            array_push($websiteIds, $websiteId);
        }

        return $storeList;
    }
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('General Setting');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('General Setting');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
