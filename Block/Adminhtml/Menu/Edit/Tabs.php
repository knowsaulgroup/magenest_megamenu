<?php
namespace Magenest\MegaMenu\Block\Adminhtml\Menu\Edit;

/**
 * Class Tabs
 * @package Magenest\YoutubeIntegration\Block\Adminhtml\Gallery\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected $_template = 'Magento_Backend::widget/tabshoriz.phtml';
    protected function _construct()
    {
        parent::_construct();
        $this->setId('template_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Menu Configuration'));
    }
}
