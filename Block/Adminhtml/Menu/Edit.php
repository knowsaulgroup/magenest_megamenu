<?php
namespace Magenest\MegaMenu\Block\Adminhtml\Menu;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container as FormContainer;
use Magento\Framework\Registry;

/**
 * Class Edit
 * @package Magenest\YoutubeIntegration\Block\Adminhtml\Gallery
 */
class Edit extends FormContainer
{
    /**
     * @var Registry
     */


    protected $_coreRegistry;

    /**
     * Edit constructor.
     * @param Registry $registry
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Registry $registry,
        Context $context,
        array $data
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'menu_id';
        $this->_blockGroup = 'Magenest_MegaMenu';
        $this->_controller = 'adminhtml_menu';
        $this->_mode       = 'edit';
        parent::_construct();
        $this->buttonList->remove('delete');
        $this->buttonList->update('save', 'label', __('Save'));

    }
    /**
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        $templates = $this->_coreRegistry->registry('magenest_mega_menu');
        if ($templates->getId()) {
            return __("Edit Rule '%1'", $this->escapeHtml($templates->getTitle()));
        }

        return __('New Rule');
    }

    /**
     * @return string
     */
    public function _getSaveAndContinueUrl()
    {
        return $this->getUrl('menu/*/save', array('_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}'));
    }
    /**
     * @return string
     */
    public function getJsLayout()
    {
        return \Zend_Json::encode($this->jsLayout);
    }
}
