<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 25/03/2016
 * Time: 15:41
 */

namespace Magenest\MegaMenu\Block\Adminhtml\Menu;

class Menu extends \Magento\Backend\Block\Template
{

    protected $_template = 'menu/themain.phtml';


    /**
     * @return string
     */
    public function getCmsUrl()
    {
        return $this->getUrl('menu/menu/cmsajax');

    }//end getCmsUrl()


    /**
     * @return string
     */
    public function getCatUrl()
    {
        return $this->getUrl('menu/menu/categoryajax');

    }//end getCatUrl()
}//end class
