/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        'ko',
        'Magenest_Megamenu/js/model/pageModel',
        'mage/translate'
    ],
    function (
        $,
        _,
        Component,
        ko,
        pageModel,
        $t
    ) {
        'use strict';

        /** Set payment methods to collection */
      //  paymentService.setPaymentMethods(methodConverter(window.checkoutConfig.paymentMethods));

        return Component.extend({
            defaults: {
                template: 'Magenest_Megamenu/megamenu',
                activeMethod: ''
            },

            initialize: function () {
                this._super();

                return this;
            },

            navigate: function () {
               console.log('navigate');
            }
        });
    }
);
