/**
 * Created by thuy on 17/04/2017.
 */
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        "underscore",
        'uiComponent',
        'ko',
        'pageModel',
        'catModel',
        'Menu',
        'Magenest_MegaMenu/js/model/menuHandler',
        'mage/translate'
    ],
    function ($,
              _,
              Component,
              ko,
              pageModel,
              catModel,
              Menu,
              menuHandler,
              $t) {
        'use strict';
        $(document).on("click", ".mega-fieldset-title", function () {
            $(this).parent(".mega-fieldset").toggleClass("active");
        });

        return Component.extend({
            defaults: {
                scope: 'megaMenu',
                template: 'Magenest_MegaMenu/megamenu',
            },
            menuId : 0,
            menuName: '',
            storeId: 1,
            isActive: 1,
            pages: [],
            selectedPage: [],
            chosenPages: [],
            currentMenuItem:'',
            saveUrl: 'http://localhost',
            formKey: 'formKey',
            cats: [],
            selectedCats: [],
            chosenCats: [],
            custom_link_title: '',
            custom_link_link: '',
            menus: [],
            removedItems: [],
            serializedRemovedItems :'',

            /**
             * init data for the menu
             */
            initObservable: function () {
                this._super()
                    .observe({
                        isLoading: true,
                        menuId:0,
                        menuName: '',
                        storeId: 1,
                        isActive: 1,
                        selectedPage: [],
                        chosenPages: [],
                        saveUrl: '',
                        formKey: '',
                        pages: [],
                        cats: [],
                        selectedCats: [],
                        chosenCats: [],
                        currentMenuItem: [],
                        custom_link_title: '',
                        custom_link_link: '',
                        menus: [],
                        hasBrother: true,
                        removedItems: [],
                        serializedRemovedItems: ''
                    });

                var self = this;
                var menuConfig = window.megaMenuConfig;
                // self.hasBrother(hasBrother);

                self.saveUrl(menuConfig.save_url);
                //show page
                var mappedPages = $.map(menuConfig.pages, function (item) {
                    var pageObj = new pageModel(item);
                    pageObj.parentLevel1 = self;
                    return pageObj
                });

                self.pages(mappedPages);
                //show cat
                var mappedCats = $.map(menuConfig.cats, function (item) {
                    var catObj = new catModel(item);
                    catObj.parentLevel1 = self;
                    return catObj
                });
                self.cats(mappedCats);


                //init the menu items
                var mappedMenu = $.map(menuConfig.menu_item, function (item) {
                    var menuObj = new Menu(item);
                    menuObj.parentLevel1 = self;
                    return menuObj;

                });

                self.menus(mappedMenu);


                /** init the currentMenu which is a empty menu item */

                var mockMenuItemData = {};

                self.currentMenuItem(new Menu(mockMenuItemData));

                self.serializedRemovedItems = ko.pureComputed(function () {

                    var serializedr = '';
                    ko.utils.arrayForEach(self.removedItems(), function (item, i) {
                        if (i === 0) {
                            serializedr = item + ',';
                        } else if (i > 0 && i < self.removedItems().length - 1) {
                            serializedr = serializedr + item + ',';
                        } else {
                            serializedr = serializedr + item;
                        }
                    });
                    return serializedr;
                }, self);

                self.bindAction();
                return this;
            },
            bindAction: function () {
                $('#save').off('click');


                $('#save').on('click', function () {

                    self.updateOrder();
                });

            },

            navigate: function () {
            },

            checkPage: function ($page) {
                var self = this;
                if (self.selectedPage == undefined) self = this.parentLevel1;
                self.selectedPage().length = 0;

                var menuIds = $.map(self.menus(), function (menu) {
                    return menu.id();
                });
                var maxId;
                if (menuIds.length == 0) {
                    var maxId = 1;
                } else {
                    var maxId = Math.max.apply(this, menuIds);
                    maxId++;
                }

                var page_id = ko.unwrap($page.page_id);
                var name,link;
                ko.utils.arrayForEach(self.chosenPages(), function (id) {
                    ko.utils.arrayForEach(self.pages(), function (pid) {
                        if (id == pid.page_id()) {
                            name = pid.title();
                            link = pid.link();
                        }
                    });
                    self.selectedPage.push({id: maxId, name: name, link:link,parent: 0, level: 0, type: 'page', obj_id: id});
                    maxId++;
                });


                return true;
            },
            btnAddPage: function (data, event) {
                var self = this;
                ko.utils.arrayForEach(self.selectedPage(), function (page) {
                    var menuItem = new Menu({
                        id: page.id,
                        is_new: true,
                        title: page.name,
                        link: page.link,
                        level: 0,
                        sort: 100,
                        hasParent: false,
                        type: page.type,
                        obj_id: page.obj_id
                    });
                    menuItem.parentLevel1 = self;

                    self.menus.push(menuItem);

                });

                self.updateOrder();
                self.updateTreeRelation();
            },
            //check cat
            checkCat: function ($cat) {
                var self = this;
                if (self.selectedCats == undefined) self = this.parentLevel1;
                self.selectedCats().length = 0;

                var menuIds = $.map(self.menus(), function (menu) {
                    return menu.id();
                });
                var maxId;
                if (menuIds.length == 0) {
                    var maxId = 1;
                } else {
                    var maxId = Math.max.apply(this, menuIds);
                    maxId++;
                }

                var cat_id = ko.unwrap($cat.cat_id);
                var name,link;
                ko.utils.arrayForEach(self.chosenCats(), function (id) {
                    ko.utils.arrayForEach(self.cats(), function (pid) {
                        if (id == pid.cat_id()){
                            name = pid.title();
                            link = pid.link();
                        }
                    });
                    self.selectedCats.push({id: maxId, name: name, link: link, parent: 0, level: 0, type: 'cat', obj_id: id});
                    maxId++;
                });
                return true;
            },
            //add cat
            btnAddCat: function (data, event) {
                var self = this;
                ko.utils.arrayForEach(self.selectedCats(), function (cat) {

                    var newAddedCat = new Menu({
                        id: cat.id,
                        is_new: true,
                        title: cat.name,
                        link: cat.link,
                        level: 0,
                        sort: 100,
                        hasParent: false,
                        type: cat.type,
                        obj_id: cat.obj_id
                    });

                    newAddedCat.parentLevel1 = self;

                    self.menus.push(newAddedCat);

                });

                self.updateOrder();

                self.updateTreeRelation();
            },
            //add customlink
            btnCustomLink: function (data, event) {
                var self = this;
                var menuIds = $.map(self.menus(), function (menu) {
                    return menu.id();
                });
                var maxId;
                if (menuIds.length == 0) {
                    var maxId = 1;
                } else {
                    var maxId = Math.max.apply(this, menuIds);
                    maxId++;
                }


                var linkMenu = new Menu({
                    id: maxId,
                    is_new: true,
                    title: self.custom_link_title(),
                    // link: page.link,
                    level: 0,
                    sort: 100,
                    hasParent: false,
                    type: 'link',
                    obj_id: 0,
                    link: self.custom_link_link()
                });

                linkMenu.parentLevel1 = self;
                self.menus.push(linkMenu);

                self.updateTreeRelation();
            },

            /**
             * Remove item in menu
             * @param menu
             * @param event
             */
            removeMenu: function (menu, event) {

                var self = menu.parentLevel1;
                event.stopPropagation();

                //need to remove the menu and all its childrens
                var menuId = menu.id();

                var currentLevel = menu.level();

                //find the down limit of the menu
                var targetSelector = 'li[data-id=' + '"' + menuId + '"]';
                var targetLi = $(targetSelector).first();

                var nextElementLimitor = targetLi.nextUntil('ul').filter(function (index, element) {
                    var level = parseInt($(this).attr('data-level'));
                    if (level == currentLevel || level < currentLevel) {
                        return true;
                    }
                }).first();

                if (nextElementLimitor.length > 0) {
                    var childrensOfLi = targetLi.nextUntil(nextElementLimitor).each(function () {
                        var childrenId = $(this).data('id');
                        ko.utils.arrayForEach(self.menus(), function (item) {

                            if (item.id() == childrenId) {
                                self.removedItems.push(item.id());
                                self.menus.destroy(item);

                            }

                        });
                    });
                }

                //if the removed item has childrens and it is in the bottom
                if (nextElementLimitor.length == 0) {
                    var childrensOfLi = targetLi.nextUntil('ul').filter(function (index, element) {
                        var level = parseInt($(this).attr('data-level'));
                        if (level > currentLevel) {
                            return true;
                        }
                    });

                    if (childrensOfLi.length > 0) {
                        var childrenId = $(this).data('id');
                        ko.utils.arrayForEach(self.menus(), function (item) {

                            if (item.id() == childrenId) {
                                self.removedItems.push(item.id());
                                self.menus.destroy(item);

                            }

                        });
                    }

                }

                self.removedItems.push(menu.id());
                self.menus.destroy(menu);
                self.updateTreeRelation();

            },

            editMenu: function (menu) {

                var self = menu.parentLevel1;

                //turn off isSelected for another menu item
                ko.utils.arrayForEach(self.menus(),function (item) {
                    item.isSelected(false);

                });

                menu.isSelected(true);
            },

            /**
             * update the sort attribute of menu item whenever the order between them changed
             */
            updateOrder : function () {
                var self = this;

                var lengthOfLi =  $('ul[data-role="menu-container"] li').length;
                $('ul[data-role="menu-container"] li').each(function(i,e) {

                    //if it is top li then it can go up
                    if (i ==0) {

                       var idOfMenuItem =  $(e).find('input[data-role="id"]').val();

                       ko.utils.arrayForEach(self.menus(), function (item) {

                           if (item.id() == idOfMenuItem) {
                               item.isTop(true);
                               item.sort(0);

                               //if it is not the level 0 then make it level 0
                               var levelOfFirstItem = parseInt(item.level());

                               if (levelOfFirstItem > 0) {
                                   var movePace = levelOfFirstItem;
                                   item.moveMenuItemBySomeLevel(movePace,item);

                               }
                           }
                       });
                    } else {

                        var idOfMenuItem =  $(e).find('input[data-role="id"]').val();
                        ko.utils.arrayForEach(self.menus(), function (item) {
                            item.isTop(false);
                            item.isBottom(false);
                            if (item.id() == idOfMenuItem) {
                                item.sort(i);
                            }
                        });
                    }

                    //last item
                    if (lengthOfLi > 0) {
                        var idOfMenuItem = $('ul[data-role="menu-container"] li').last().find('input[data-role="id"]').val();

                        ko.utils.arrayForEach(self.menus(), function (item) {

                            if (item.id() == idOfMenuItem) {
                                item.isBottom(true);
                            }
                        });
                    }

                    var updatedOrder = i + 1;
                    var eInputOrder = $(e).find('input[data-role="sort"]');
                    var orderVal = $(eInputOrder).val(updatedOrder);
                });
            },

            save: function () {
              $('#menu_form').submit();
            },



            /**
             * update the relation between elements
             */
            updateTreeRelation: function() {
                var self = this;
                $('ul[data-role="menu-container"]').find('li').each(function (index,element) {

                    var menuId = $(element).data('id');
                    var menuItem ;
                    
                    ko.utils.arrayForEach(self.menus(), function (item) {
                        if (item.id() == menuId) {
                            menuItem = item;

                            //update the parent id and parent name
                            var parentElement = menuItem.getParentLi(menuItem);

                            if (parentElement.length > 0) {
                                var parent_name = parentElement.data('title');
                                var parent_id = parseInt(parentElement.data('id'));

                                if (isNaN(parent_id)) parent_id = 0;

                                item.parentName(parent_name);
                                item.parent(parent_id);
                            } else {
                                item.parentName('');
                                item.parent(0);
                            }

                            //update the brother relationship
                            var brotherElement = menuItem.getBrotherLi(menuItem);

                            if (brotherElement.length > 0) {
                                var brother_name = brotherElement.data('title');
                                var brother_id = parseInt(brotherElement.data('id'));

                                if (isNaN(brother_id)) brother_id = 0;

                                item.brother_name(brother_name);
                                item.brother_id(brother_id);
                                item.hasBrother(true);
                            } else {
                                item.brother_name('');
                                item.brother_id(0);
                                item.hasBrother(false);

                            }

                        }
                        
                    });
                    
                });
            },
            expandAll: function () {
                $('.ui-accordion-header').removeClass('ui-corner-all').addClass('ui-accordion-header-active ui-state-active ui-corner-top').attr({
                    'aria-selected': 'true',
                    'tabindex': '0'
                });
                $('.ui-accordion-header .ui-icon').removeClass('ui-icon-triangle-1-e').addClass('ui-icon-triangle-1-s');
                $('.ui-accordion-content').addClass('ui-accordion-content-active').attr({
                    'aria-expanded': 'true',
                    'aria-hidden': 'false'
                }).show();
            },
            collapseAll: function () {
                $('.ui-accordion-header').removeClass('ui-accordion-header-active ui-state-active ui-corner-top').addClass('ui-corner-all').attr({
                    'aria-selected': 'false',
                    'tabindex': '-1'
                });
                $('.ui-accordion-header .ui-icon').removeClass('ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-e');
                $('.ui-accordion-content').removeClass('ui-accordion-content-active').attr({
                    'aria-expanded': 'false',
                    'aria-hidden': 'true'
                }).hide();
            },
            hasBrother: function () {

            }
        });
    }
);
