/**
 * Created by thuy on 03/06/2017.
 */
/**
 * Created by thuy on 12/04/2017.
 */
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'ko',
        'Magenest_MegaMenu/js/action/refreshPageModelAction',
        'colorpicker'
    ],
    function ($, ko, refreshPageModelAction, mmcolorpicker) {
        return function (data) {
            var self = this;
            var isGrid = false;
            var isContent = false;
            var isColumn = false;
            if ((typeof data.childrenraw !== 'undefined')|| data.mainContentHtml !== null) {
                // if(data.mainEnable === null||data.mainEnable === '0'){
                    data.mainEnable = '1';
                    data.showMain = true;
                // }
                if(data.mainContentType === null || data.mainContentType === 'grid' ){
                    data.mainContentType = 'grid';
                    isGrid = true;
                    isContent = false;
                    isColumn = true;

                }
                if(data.mainColumn === null){
                    data.mainColumn = 4;
                }
            }
            else{
                data.mainEnable = '0';
                data.showMain = false;
                data.mainContentType = null;
            }
            if(data.mainContentType === 'grid'){
                isGrid = true;
                isContent = false;
            }
            else if(data.mainContentType === 'content'){
                isGrid = false;
                isContent = true;
            }
            else{
                isGrid = false;
                isContent = false;
            }
            if(data.leftEnable === null || data.leftEnable === '0'){
                data.showLeft = false;
                data.leftEnable = '0';
            }
            if(data.rightEnable === null || data.rightEnable === '0'){
                data.showRight = false;
                data.rightEnable = '0';
            }
            // else if(this.mainContentType() === 'content'){
            //     this.showGrid(false);
            //     this.showContent(true);
            // }
            // else {
            //     this.showGrid(false);
            //     this.showContent(false);
            // }
            return {
                title: ko.observable(data.title),
                id: ko.observable(data.id),
                sort: ko.observable(data.sort),
                level: ko.observable(data.level),
                levelTemp: ko.observable(1),
                parent: ko.observable(data.parent),
                parentNode: ko.observable(data.parentNode),
                is_new: ko.observable(data.is_new),
                type: ko.observable(data.type),
                obj_id: ko.observable(data.obj_id),
                hasBrother: ko.observable(data.hasBrother),
                hasParent: ko.observable(data.hasParent),
                megaColumn: ko.observable(data.megaColumn),
                link: ko.observable(data.link),
                brother_name: ko.observable(data.brother_name),
                brother_id: ko.observable(data.brother_id),
                parent_name: ko.observable(data.parent_name),
                parentName: ko.observable(data.parentName),
                include_child: ko.observable(data.include_child),
                open_setting: ko.observable('in-active'),
                needUpdateBrother: ko.observable(false),

                isSelected: ko.observable(false),
                cssClass: ko.observable(data.cssClass),
                showIcon: ko.observable(data.showIcon),

                /** Attribute about position and relation with brother and parent */
                isTop : ko.observable(false),
                isBottom : ko.observable(false),
                children : ko.observableArray(),
                flatChildren: ko.observableArray(),
                width: ko.observable(data.width),
                //contentHtml: ko.observable(data.contentHtml),

                /** Left Block Attribute **/
                leftEnable : ko.observable(data.leftEnable),
                leftClass: ko.observable(data.leftClass),

                leftWidth : ko.observable(data.leftWidth),
                leftContentHtml : ko.observable(data.leftContentHtml),


                /** Main Block Content Attribute **/
                mainEnable : ko.observable(data.mainEnable),
                mainProductSku : ko.observable(data.mainProductSku),
                mainInCategory : ko.observable(data.mainInCategory),

                mainContentType : ko.observable(data.mainContentType),
                // if(mainContentType == null){
                //
                // }
                mainColumn : ko.observable(data.mainColumn),

                mainContentHtml : ko.observable(data.mainContentHtml),

                /** Right Block **/
                rightEnable : ko.observable(data.rightEnable),
                rightClass : ko.observable(data.rightClass),

                rightWidth : ko.observable(data.rightWidth),
                rightContentHtml : ko.observable(data.rightContentHtml),

                /** Style Attribute **/
                textColor : ko.observable(data.textColor),
                hoverTextColor : ko.observable(data.hoverTextColor),
                serializedChildren : ko.observable(),

                /** Style main content**/
                showGrid : ko.observable(isGrid),
                showContent : ko.observable(isContent),
                showMain : ko.observable(data.showMain),
                showLeft : ko.observable(data.showLeft),
                showRight : ko.observable(data.showRight),

                getSerializeChildren: function() {
                    var self = this;
                    var out =0;
                    ko.utils.arrayForEach(self.flatChildren(),function (item) {
                            out += ',' +  item;
                    });
                    this.serializedChildren(out);
                   // return out;
                },
                setImageSource: function (title) {
                    this.title(title);
                },
                getTitle: function () {
                    return this.title;
                },
                getLink: function () {
                    return this.link;
                },
                setLink: function (link) {
                    this.link(link);
                },
                myEdit : function () {
                    var self = this;
                    ko.utils.arrayForEach(self.parentLevel1.nestedMenu() , function (item) {
                        item.isSelected(false);
                    });


                    self.isSelected(true);

                },
                myRemove : function () {
                    var menu = this;
                    var menuId = menu.id();
                    ko.utils.arrayForEach(menu.parentLevel1.nestedMenu() , function (item) {
                        menu.parentLevel1.removedItems.push(menuId);
                        menu.parentLevel1.nestedMenu.remove(menu);
                    });
                },
                detachChild: function(menuId,parentId) {

                    //find the parent li element

                    var menu = this;
                    ko.utils.arrayForEach(menu.parentLevel1.nestedMenu() , function (item) {
                        if (item.id() == parentId ) {
                            ///
                            ko.utils.arrayForEach(item.children() , function( child) {
                                if (child.id() == menuId) {
                                    item.children.remove(child);
                                }
                            });

                        }

                    });
                },
                eventMainxxx: function (data, event) {
                    // $('select[data-bind="value:mainContentType"]').on('change', function () {
                        if(this.mainContentType() === 'grid'){
                            this.showGrid(true);
                            this.showContent(false);
                        }
                        else if(this.mainContentType() === 'content'){
                            this.showGrid(false);
                            this.showContent(true);
                        }
                        else {
                            this.showGrid(false);
                            this.showContent(false);
                        }
                },
                eventMainEnable: function () {
                    if(this.mainEnable() === '1'){
                        this.showMain(true);
                    }
                    else {
                        this.showMain(false);
                        this.showGrid(false);
                        this.showContent(false);
                    }
                },
                eventSideLeftEnable: function () {
                    if(this.leftEnable() === '1'){
                        this.showLeft(true);
                    }
                    else this.showLeft(false);
                },
                eventSideRightEnable: function () {
                    if(this.rightEnable() === '1'){
                        this.showRight(true);
                    }
                    else this.showRight(false);
                }


            };
        }
    }
);