/**
 * Created by thuy on 12/04/2017.
 */
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'ko',
        'Magenest_MegaMenu/js/action/refreshPageModelAction'
    ],
    function ($, ko, refreshPageModelAction) {
        return function (data) {
            return {
                title: ko.observable(data.title),
                id: ko.observable(data.id),
                sort: ko.observable(data.sort),
                level: ko.observable(data.level),
                parent: ko.observable(data.parent),
                parentNode: ko.observable(data.parentNode),
                is_new: ko.observable(data.is_new),
                type: ko.observable(data.type),
                obj_id: ko.observable(data.obj_id),
                hasBrother: ko.observable(data.hasBrother),
                hasParent: ko.observable(data.hasParent),
                megaColumn: ko.observable(data.megaColumn),
                link: ko.observable(data.link),
                brother_name: ko.observable(data.brother_name),
                brother_id: ko.observable(data.brother_id),
                parent_name: ko.observable(data.parent_name),
                parentName: ko.observable(data.parentName),
                include_child: ko.observable(data.include_child),
                open_setting: ko.observable('in-active'),
                needUpdateBrother: ko.observable(false),

                isSelected: ko.observable(false),
                cssClass: ko.observable(data.cssClass),
                showIcon: ko.observable(data.showIcon),

                /** Attribute about position and relation with brother and parent */
                isTop : ko.observable(false),
                isBottom : ko.observable(false),
                children : ko.observableArray(),


                width: ko.observable(data.width),
                //contentHtml: ko.observable(data.contentHtml),

                /** Left Block Attribute **/
                leftEnable : ko.observable(data.leftEnable),
                leftClass: ko.observable(data.leftClass),

                leftWidth : ko.observable(data.leftWidth),
                leftContentHtml : ko.observable(data.leftContentHtml),


                /** Main Block Content Attribute **/
                mainSubProduct : ko.observable(data.mainSubProduct),
                mainProductSku : ko.observable(data.mainProductSku),
                mainInCategory : ko.observable(data.mainInCategory),

                mainContentType : ko.observable(data.mainContentType),
                mainColumn : ko.observable(data.mainColumn),

                mainContentHtml : ko.observable(data.mainContentHtml),

                /** Right Block **/
                rightEnable : ko.observable(data.rightEnable),
                rightClass : ko.observable(data.rightClass),

                rightWith : ko.observable(data.rightWith),
                rightContentHtml : ko.observable(data.rightContentHtml),

                /** Style Attribute **/
                textColor : ko.observable(data.textColor),
                hoverTextColor : ko.observable(data.hoverTextColor),




                setImageSource: function (title) {
                    this.title(title);
                },
                getTitle: function () {
                    return this.title;
                },
                getLink: function () {
                    return this.link;
                },
                setLink: function (link) {
                    this.link(link);
                },

                downOneCol: function (menu, event) {
                    event.stopPropagation();
                    var oldValue = parseInt(ko.unwrap(menu.megaColumn));
                    if (isNaN(oldValue)) oldValue = 0;
                    if (oldValue > 2)
                        menu.megaColumn(oldValue - 1);

                },
                upOneCol: function (menu, event) {
                    event.stopPropagation();
                    var oldValue = parseInt(ko.unwrap(menu.megaColumn));
                    if (isNaN(oldValue)) oldValue = 0;
                    if (oldValue < 12)
                        menu.megaColumn(oldValue + 1);

                },

                //go under brother menu
                outParent : function (menu,event) {
                    var self = this;
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);

                    menu.updateChildren(targetLi ,menu);

                    menu.level( parseInt(menu.level()) - 1);

                    ko.utils.arrayForEach(menu.children() , function (itemId) {
                        ko.utils.arrayForEach(menu.parentLevel1.menus(), function (item) {
                            if (item.id() == itemId) {
                                item.level(parseInt( item.level()) - 1);
                            }

                        }) ;

                    });
                    self.parentLevel1.updateTreeRelation();
                    self.parentLevel1.updateOrder();
                },
                /**
                 * menu item become children of its brother
                 * @param menu
                 * @param event
                 */
                goUnder : function (menu,event) {
                    var self = this;
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);

                    menu.updateChildren(targetLi ,menu);

                    menu.level( parseInt(menu.level()) + 1);

                    ko.utils.arrayForEach(menu.children() , function (itemId) {
                        ko.utils.arrayForEach(menu.parentLevel1.menus(), function (item) {
                            if (item.id() == itemId) {
                                item.level(parseInt( item.level()) + 1);
                            }

                        }) ;

                    });
                    menu.parentLevel1.updateOrder();
                    menu.parentLevel1.updateTreeRelation();
                },

                /**
                 * Menu item and its children go down on step in menu tree\
                 * @param menu
                 * @param event
                 */
                  
                downOne: function (menu, event) {
                    var self = this;
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);

                    var nextLi = targetLi.next();

                    $('li').attr('move', '0');

                    self.updateChildren(targetLi,menu);
                    targetLi.attr('move', '1');

                    //move the element

                    $('li[move="1"]').insertAfter(nextLi);

                    //unmarked all remove data attribute
                    $('li').attr('move', '0');
                    //unmarked all remove data attribute
                    $('li').attr('move', '0');

                    menu.updateRelationship(targetLi,menu);
                    menu.parentLevel1.updateOrder();
                    menu.parentLevel1.updateTreeRelation();
                },
                myEdit: function () {
                    alert('my experimental');
                },
                /**
                 * Menu item and its children go up in the menu tree
                 * @param menu
                 * @param event
                 */
                upOne: function (menu,event) {
                    var self = this;
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);

                    var prevLi = targetLi.prev();

                    var childrenElements = '';
                    //unmarked all remove data attribute
                    $('li').attr('move', '0');

                    //marked remove attribute
                    self.updateChildren(targetLi,menu);
                    targetLi.attr('move', '1');

                    //move the element

                    $('li[move="1"]').insertBefore(prevLi);

                    //unmarked all remove data attribute
                    $('li').attr('move', '0');

                    menu.updateRelationship(targetLi,menu);

                    //update the level of all item
                    menu.parentLevel1.updateOrder();
                    menu.parentLevel1.updateTreeRelation();
                },

                /**
                 *move menu item by some level
                 * move 1: level 2 become level 1 delta is 1
                 * move 2: level 2 become level 0 delta is 2
                 * move 3 : level 2 becomes level 0 delta is 2
                 * move -1: level 2 become level 3 delta is -1
                 * 1-1 = 0
                 */
                moveMenuItemBySomeLevel: function(levelCnt,menu) {
                    var currentLevel = parseIn(tmenu.level());
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);

                    var newLevel =  currentLevel - parseInt(levelCnt);

                    if (newLevel < 0 ) newLevel = 0;

                    var delta = currentLevel - newLevel;
                    menu.level(newLevel);

                    //update children level
                    ko.utils.arrayForEach(menu.children() , function (itemId) {
                        ko.utils.arrayForEach(menu.parentLevel1.menus(), function (item) {
                            if (item.id() == itemId) {
                                item.level( item.level() - delta);
                            }

                        }) ;

                    });
                },

                /**
                 * update all children of target li
                 * with new information
                 * @param a jQuery selector element
                 * @param the menu object
                 */
                updateChildren : function (targetLi,menu) {
                    var self = this;
                    var currentLevel = targetLi.data('level');
                    var menuId = targetLi.data('id');


                    var downBarrierLi = menu.getDownBarrierLi(menu);

                    if (downBarrierLi.length > 0) {
                        var children = targetLi.nextUntil(downBarrierLi).each(function () {
                            $(this).attr('move', '1');
                            var childId = $(this).data('id');

                            //refresh the children of current li element target
                            menu.children([]);
                            menu.children.push(childId);
                        });
                    } else {
                        var children = targetLi.next('li').each(function () {
                            $(this).attr('move', '1');
                            var childId = $(this).data('id');

                            //refresh the children of current li element target
                            menu.children([]);
                            menu.children.push(childId);
                        });
                    }
                },

                /**
                 * get down barrier
                 * return null | element
                 * @param menu
                 */
                getDownBarrierLi : function (menu) {
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);
                    var currentLevel = menu.level();

                    if ($('li[downBarrier="true"]').length > 0) $('li[downBarrier="true"]').attr('downBarrier','false');

                    var downBarrierLi = targetLi.nextUntil('ul').filter(function (index, element) {
                        var level = parseInt($(this).attr('data-level'));
                        if (level == currentLevel || level < currentLevel) {
                            $(this).attr('downBarrier', 'true');
                            return true;
                        }
                    }).first();

                    return downBarrierLi;
                },
                /**
                 *
                 * @param menu
                 */

                getUpBarrierLi : function (menu) {
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);
                    var currentLevel = menu.level();

                    if ($('li[upBarrier="true"]').length > 0) $('li[downBarrier="true"]').attr('downBarrier','false');

                    var upBarrierLi = targetLi.prevUntil('ul').filter(function (index, element) {
                        var level = parseInt($(this).attr('data-level'));
                        if (level == currentLevel || level < currentLevel) {
                            $(this).attr('upBarrier', 'true');
                            return true;
                        }
                    }).first();

                    return upBarrierLi;
                },

                /**
                 * get the brother of menu item
                 * @param menu
                 */
                getBrotherLi : function (menu) {
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);
                    var currentLevel = menu.level();

                    var parentLi = menu.getParentLi(menu);

                    if (parentLi.length > 0){
                        var upBarrierLi = targetLi.prevUntil(parentLi).filter(function (index, element) {
                            var level = parseInt($(this).attr('data-level'));
                            if (level == currentLevel ) {
                                return true;
                            }
                        }).first();

                        return upBarrierLi;
                    } else {
                        var upBarrierLi = targetLi.prevUntil('ul').filter(function (index, element) {
                            var level = parseInt($(this).attr('data-level'));
                            if (level == currentLevel ) {
                                return true;
                            }
                        }).first();

                        return upBarrierLi;
                    }
                },

                /**
                 *
                 * @param menu
                 */
                getParentLi : function (menu) {
                    var menuId = menu.id();
                    var targetElementId  =  '#menuitem' + menuId;
                    var targetLi = $(targetElementId);
                    var currentLevel = menu.level();

                    if ($('li[updateParent="true"]').length > 0) $('li[updateParent="true"]').attr('updateParent','false');

                    var updatedParentLi = targetLi.prevUntil('ul').filter(function (index, element) {
                        var level = parseInt($(this).attr('data-level'));

                        if (level < currentLevel) {
                            $(this).attr('updateParent', 'true');
                            return true;
                        }
                    }).first();

                    return updatedParentLi;
                },



                /**
                 * update relationship of menu item
                 */
                updateRelationship : function (targetLi,menu) {
                    var parentLi = menu.getParentLi(menu);
                    var parentLevel = 0;
                    var parenName = '';
                    var currentLevel = menu.level();

                    var updatedLevel;
                    var deltaLevel;

                    if (parentLi.length > 0 )  {
                        parentLevel = parentLi.data('level');
                        parenName = parentLi.data('title');

                        updatedLevel = parentLevel + 1;
                        deltaLevel = parseInt(currentLevel) - parseInt(updatedLevel);
                    } else {
                        parentLevel = 0;
                        parenName = '';
                        updatedLevel= 0;
                        deltaLevel = parseInt(currentLevel) - 0;
                    }

                    menu.level(updatedLevel);
                    menu.parentName(parenName);

                    ko.utils.arrayForEach(menu.children() , function (itemId) {
                        ko.utils.arrayForEach(menu.parentLevel1.menus(), function (item) {
                            if (item.id() == itemId) {
                                item.level( item.level() + deltaLevel);

                            }

                        }) ;

                    });
                },
            };
        }
    }
);