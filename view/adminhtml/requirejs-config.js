/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            pageModel: 'Magenest_MegaMenu/js/model/pageModel',
            catModel: 'Magenest_MegaMenu/js/model/catModel',
            Menu: 'Magenest_MegaMenu/js/model/Menu',
            Nest: 'Magenest_MegaMenu/js/model/Nest'
        }
    }
    ,
    paths: {
        //jquery.nestable
        "colorpicker": "Magenest_MegaMenu/js/lib/colorpicker",
        "nestable": "Magenest_MegaMenu/js/lib/jquery.nestable"
    },
    shim: {
        'colorpicker': {
            'deps': ['jquery']
        },
        'nestable': {
            'deps': ['jquery']
        }
    }
};
