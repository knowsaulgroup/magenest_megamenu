<?php
namespace Magenest\MegaMenu\Setup;

use Magento\Eav\Setup\EavSetup;

class MenuEntitySetup extends EavSetup
{
    public function getDefaultEntities()
    {
        /*	#snippet1	*/
        $menuEntityEntity =     \Magenest\MegaMenu\Model\MenuEntity::ENTITY;
        $entities =     array(
            $menuEntityEntity   =>  array(
                'entity_model'  =>  'Magenest\MegaMenu\Model\ResourceModel\MenuEntity',
                'table'     =>  $menuEntityEntity . '_entity',
                'attributes'    =>  array(
                    'menu_id'   =>  array(
                        'type'  =>  'static',
                    ),
                    'label_name'    =>  array(
                        'type'  =>  'static',
                    ),
                    'level'     =>  array(
                        'type'  =>  'static',
                    ),
                    'menu_type'     =>  array(
                        'type'  =>  'static',
                    ),
                    'is_mega'   =>  array(
                        'type'  =>  'static',
                    ),
                ),
            ),
        );
        return  $entities;
        //end
    }
}
