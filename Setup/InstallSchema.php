<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 09/03/2016
 * Time: 15:54
 */
namespace Magenest\MegaMenu\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{


    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'magenest_mega_menu'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magenest_mega_menu'))
            ->addColumn(
                'menu_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true),
                'Menu ID'
            ) ->addColumn(
                'id_temp',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array( 'unsigned' => true, 'nullable' => false),
                'Temporary ID'
            )
            ->addColumn(
                'menu_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                64,
                array(),
                'Menu Name'
            )
            ->addColumn(
                'store_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                //                'type' , 'store',
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Store ID'
            )
            ->addColumn(
                'menu_template',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                64,
                array('unsigned' => true, 'nullable' => false, 'default' => 'horizontal_menu'),
                'menu Template'
            )
            ->addColumn(
                'menu_event',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                64,
                array('unsigned' => true, 'nullable' => false, 'default' => 'hover'),
                'Menu Event'
            )
            ->addColumn(
                'is_active',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Is Active'
            )->addColumn(
                'id_temp',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Temporary Id'
            )
            ->addIndex(
                $installer->getIdxName(
                    'magenest_mega_menu',
                    array('menu_id', 'store_id'),
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('menu_id', 'store_id'),
                array('type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE)
            )

            ->setComment('Mega Menu');
        $installer->getConnection()->createTable($table);

        /*
         *  Create table 'magenest_menu_entity'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magenest_menu_entity')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array(
             'identity' => true,
             'unsigned' => true,
             'nullable' => false,
             'primary'  => true,
            ),
            'Entity Id'
        )->addColumn(
            'menu_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            array('unsigned' => true),
            'Menu Id'
        )
            ->addIndex(
                $installer->getIdxName(
                    'magenest_menu_entity',
                    array('entity_id', 'menu_id'),
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('entity_id', 'menu_id'),
                array('type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE)
            )
            ->addIndex(
                $installer->getIdxName('megenest_menu_entity', array('menu_id')),
                array('menu_id')
            );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'maganest_menu_entity_datetime'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magenest_menu_entity_datetime'))
            ->addColumn(
                'value_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('identity' => true, 'nullable' => false, 'primary' => true),
                'Value ID'
            )
            ->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Attribute ID'
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Entity ID'
            )
            ->addColumn(
                'value',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
                null,
                array(),
                'Value'
            )
            ->addIndex(
                $installer->getIdxName(
                    'magenest_menu_entity_datetime',
                    array('entity_id', 'attribute_id'),
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('entity_id', 'attribute_id'),
                array('type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE)
            )
            ->addIndex(
                $installer->getIdxName('magenest_menu_entity_datetime', array('entity_id')),
                array('entity_id')
            )
            ->addIndex(
                $installer->getIdxName('magenest_menu_entity_datetime', array('attribute_id')),
                array('attribute_id')
            )
            ->addForeignKey(
                $installer->getFkName(
                    'magenest_menu_entity_datetime',
                    'attribute_id',
                    'eav_attribute',
                    'attribute_id'
                ),
                'attribute_id',
                $installer->getTable('eav_attribute'),
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    'magenest_menu_entity_datetime',
                    'entity_id',
                    'magenest_menu_entity',
                    'entity_id'
                ),
                'entity_id',
                $installer->getTable('magenest_menu_entity'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->setComment('Magenest MegaMenu Datetime Attribute Backend Table');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magenest_menu_entity_decimal'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magenest_menu_entity_decimal'))
            ->addColumn(
                'value_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('identity' => true, 'nullable' => false, 'primary' => true),
                'Value ID'
            )
            ->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Attribute ID'
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Entity ID'
            )
            ->addColumn(
                'value',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                array(),
                'Value'
            )
            ->addIndex(
                $installer->getIdxName(
                    'magenest_menu_entity_decimal',
                    array('entity_id', 'attribute_id'),
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('entity_id', 'attribute_id'),
                array('type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE)
            )
            ->addIndex(
                $installer->getIdxName('magenest_menu_entity_decimal', array('attribute_id')),
                array('attribute_id')
            )
            ->addForeignKey(
                $installer->getFkName(
                    'magenest_menu_entity_decimal',
                    'attribute_id',
                    'eav_attribute',
                    'attribute_id'
                ),
                'attribute_id',
                $installer->getTable('eav_attribute'),
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    'magenest_menu_entity_decimal',
                    'entity_id',
                    'magenest_menu_entity',
                    'entity_id'
                ),
                'entity_id',
                $installer->getTable('magenest_menu_entity'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->setComment('Magenest MegaMenu Decimal Attribute Backend Table');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magenest_menu_entity_int'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magenest_menu_entity_int'))
            ->addColumn(
                'value_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('identity' => true, 'nullable' => false, 'primary' => true),
                'Value ID'
            )
            ->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Attribute ID'
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Entity ID'
            )
            ->addColumn(
                'value',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array(),
                'Value'
            )
            ->addIndex(
                $installer->getIdxName(
                    'magenest_menu_entity_int',
                    array('entity_id', 'attribute_id'),
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('entity_id', 'attribute_id'),
                array('type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE)
            )
            ->addIndex(
                $installer->getIdxName('magenest_menu_entity_int', array('attribute_id')),
                array('attribute_id')
            )
            ->addIndex(
                $installer->getIdxName('magenest_menu_entity_int', array('entity_id')),
                array('entity_id')
            )
            ->addForeignKey(
                $installer->getFkName('magenest_menu_entity_int', 'attribute_id', 'eav_attribute', 'attribute_id'),
                'attribute_id',
                $installer->getTable('eav_attribute'),
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName('entity_id_entity_int', 'entity_id', 'magenest_menu_entity', 'entity_id'),
                'entity_id',
                $installer->getTable('magenest_menu_entity'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->setComment('Magenest Megeamenu Integer Attribute Backend Table');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magenest_menu_entity_text'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magenest_menu_entity_text'))
            ->addColumn(
                'value_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('identity' => true, 'nullable' => false, 'primary' => true),
                'Value ID'
            )
            ->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Attribute ID'
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Entity ID'
            )
            ->addColumn(
                'value',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '64k',
                array(),
                'Value'
            )
            ->addIndex(
                $installer->getIdxName(
                    'magenest_menu_entity_text',
                    array('entity_id', 'attribute_id'),
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('entity_id', 'attribute_id'),
                array('type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE)
            )
            ->addIndex(
                $installer->getIdxName('magenest_menu_entity_text', array('attribute_id')),
                array('attribute_id')
            )
            ->addIndex(
                $installer->getIdxName('magenest_menu_entity_text', array('entity_id')),
                array('entity_id')
            )
            ->addForeignKey(
                $installer->getFkName(
                    'magenest_menu_entity_text',
                    'attribute_id',
                    'eav_attribute',
                    'attribute_id'
                ),
                'attribute_id',
                $installer->getTable('eav_attribute'),
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    'magenest_menu_entity_text',
                    'entity_id',
                    'magenest_menu_entity',
                    'entity_id'
                ),
                'entity_id',
                $installer->getTable('magenest_menu_entity'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )

            ->setComment('Magenest Text Attribute Backend Table');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magenest_menu_entity_varchar'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magenest_menu_entity_varchar'))
            ->addColumn(
                'value_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('identity' => true, 'nullable' => false, 'primary' => true),
                'Value ID'
            )
            ->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Attribute ID'
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('unsigned' => true, 'nullable' => false, 'default' => '0'),
                'Entity ID'
            )
            ->addColumn(
                'value',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                array(),
                'Value'
            )
            ->addIndex(
                $installer->getIdxName(
                    'magenest_menu_entity_varchar',
                    array('entity_id', 'attribute_id'),
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('entity_id', 'attribute_id'),
                array('type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE)
            )
            ->addIndex(
                $installer->getIdxName('magenest_menu_entity_varchar', array('attribute_id')),
                array('attribute_id')
            )
            ->addForeignKey(
                $installer->getFkName(
                    'magenest_menu_entity_varchar',
                    'attribute_id',
                    'eav_attribute',
                    'attribute_id'
                ),
                'attribute_id',
                $installer->getTable('eav_attribute'),
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->addForeignKey(
                $installer->getFkName(
                    'magenest_menu_entity_varchar',
                    'entity_id',
                    'magenest_menu_entity',
                    'entity_id'
                ),
                'entity_id',
                $installer->getTable('magenest_menu_entity'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->setComment('Catalog Product Varchar Attribute Backend Table');
        $installer->getConnection()->createTable($table);


        $installer->endSetup();

    }//end install()
}//end class
