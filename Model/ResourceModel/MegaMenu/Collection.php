<?php
namespace Magenest\MegaMenu\Model\ResourceModel\MegaMenu;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'menu_id';
    protected function _construct()
    {
        $this->_init(
            'Magenest\MegaMenu\Model\MegaMenu',
            'Magenest\MegaMenu\Model\ResourceModel\MegaMenu'
        );
    }
}
