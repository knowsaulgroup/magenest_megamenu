<?php
namespace Magenest\MegaMenu\Model\ResourceModel;

class MenuEntity extends \Magento\Eav\Model\Entity\AbstractEntity
{
    protected function _construct()
    {
        $this->_read =  'magenest_menu_read';
        $this->_write =     'magenest_menu_write';
    }
    public function getEntityType()
    {
        if (empty($this->_type)) {
            $this->setType(\Magenest\MegaMenu\Model\MenuEntity::ENTITY);
        }

        return  parent::getEntityType();
    }
}
