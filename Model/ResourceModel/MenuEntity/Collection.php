<?php
namespace Magenest\MegaMenu\Model\ResourceModel\MenuEntity;

class Collection extends \Magento\Eav\Model\Entity\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Magenest\MegaMenu\Model\MenuEntity',
            'Magenest\MegaMenu\Model\ResourceModel\MenuEntity'
        );
    }
}
