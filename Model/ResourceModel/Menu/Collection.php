<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 09/03/2016
 * Time: 15:51
 */
namespace Magenest\MegaMenu\Model\ResourceModel\Menu;

class Collection extends \Magento\Eav\Model\Entity\Collection\AbstractCollection
{


    /**
     * Model initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\MegaMenu\Model\MenuEntity', 'Magenest\MegaMenu\Model\ResourceModel\MenuEntity');

    }//end _construct()
}//end class
