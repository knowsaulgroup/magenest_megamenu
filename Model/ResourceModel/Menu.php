<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 09/03/2016
 * Time: 15:50
 */
namespace Magenest\MegaMenu\Model\ResourceModel;

class Menu extends \Magento\Framework\Model\ResourceModel\Db\VersionControl\AbstractDb
{

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'menu_resource';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'menu';

    protected $stateHandler;


    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_menu_entity', 'entity_id');

    }//end _construct()
}//end class
