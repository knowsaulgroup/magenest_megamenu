<?php
namespace Magenest\MegaMenu\Model\ResourceModel;

class MegaMenu extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('magenest_mega_menu', 'menu_id');
    }
}
