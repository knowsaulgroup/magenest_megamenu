<?php
namespace Magenest\MegaMenu\Model;

class MenuEntity extends \Magento\Framework\Model\AbstractModel
{
    const ENTITY =  'magenest_menu';

    protected $treeInfo;


    public function _construct()
    {
        $this->     _init('Magenest\MegaMenu\Model\ResourceModel\MenuEntity');
    }

    public function getLevel0Items($menuId)
    {
        $collection =  $this->getCollection()->addFieldToFilter('level', '0')
            ->setOrder(
                'sort',
                \Magento\Framework\DB\Select::SQL_ASC
            )
            ->load();
        return $collection;
    }

    public function getF1Children()
    {
        $children = explode(',', $this->getData('children'));

        if (!empty($children)) {
            $collection =  $this->getCollection()->addFieldToFilter('entity_id', array('in'=> $children))
                ->setOrder(
                    'sort',
                    \Magento\Framework\DB\Select::SQL_ASC
                )
                ->load();
            return $collection;
        }
    }

    /**
     * @return mixed
     * @return mixed
     */
    public function getChildrenTreeFormat()
    {
        $this->load($this->getId());
        $this->treeInfo['id'] = $this->getId();
        $this->treeInfo['title'] = $this->getTitle();
        $this->treeInfo['level'] = $this->getData('level');
        $this->treeInfo['mainContentType'] = $this->getData('mainContentType');
        $this->treeInfo['mainColumn'] = $this->getData('mainColumn');
        $this->treeInfo['mainContentHtml'] = $this->getData('mainContentHtml');
        $this->treeInfo['cssClass'] = $this->getData('cssClass');
        $this->treeInfo['leftClass'] = $this->getData('leftClass');
        $this->treeInfo['leftWidth'] = $this->getData('leftWidth');
        $this->treeInfo['leftContentHtml'] = $this->getData('leftContentHtml');
        $this->treeInfo['rightClass'] = $this->getData('rightClass');
        $this->treeInfo['rightWidth'] = $this->getData('rightWidth');
        $this->treeInfo['rightContentHtml'] = $this->getData('rightContentHtml');
        $this->treeInfo['textColor'] = $this->getData('textColor');
        $this->treeInfo['hoverTextColor'] = $this->getData('hoverTextColor');
        $this->treeInfo['mainEnable'] = $this->getData('mainEnable');
        $this->treeInfo['leftEnable'] = $this->getData('leftEnable');
        $this->treeInfo['rightEnable'] = $this->getData('rightEnable');
        $this->treeInfo['link'] = $this->getLink();
//        $this->treeInfo['mainColumn'] = $this->getData('mainColumn');
        $haschildrenInfo = $this->getData('children');

        if ($haschildrenInfo =='' || $haschildrenInfo =='0') {
            $this->treeInfo['hasChild'] = 'no';
        } else {
            $this->treeInfo['hasChild'] = 'yes';
        }


        $menus = $this->getF1Children();

        if ($menus->getSize() > 0) {
            foreach ($menus as $theMenu) {
                //if ($theMenu->hasChildren()) {
                    $childrenInfo = $theMenu->getChildrenTreeFormat();
                    $this->treeInfo['childrenraw'][] = $childrenInfo;
                //}
            }
        }

        return $this->treeInfo;

    }//end getChildrenRecu()
}
