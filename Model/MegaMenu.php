<?php
namespace Magenest\MegaMenu\Model;

class MegaMenu extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->     _init('Magenest\MegaMenu\Model\ResourceModel\MegaMenu');
    }
}
