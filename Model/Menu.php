<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 09/03/2016
 * Time: 15:47
 */
namespace Magenest\MegaMenu\Model;

class Menu extends \Magento\Framework\Model\AbstractExtensibleModel
{
    protected $_allChildren;

    protected $productCollectionFactory;

    protected $catalogProductVisibility;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    protected $catalogCategoryHelper;

    protected $categoryRepo;

    protected $pageHelper;

    protected $categoryFactory;

    protected $catalogConfig;

    protected $imageBuilder;


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Catalog\Block\Product\Context $catalogContext,
        \Magento\Catalog\Helper\Category $catalogCategory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Cms\Helper\Page $pageHelper,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = array()
    ) {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $registry = $objectManager->create('\Magento\Framework\Registry');
        $localeDate = $objectManager->create('\Magento\Framework\Stdlib\DateTime\TimezoneInterface');
        $this->_eavConfig    = $eavConfig;

        $this->productCollectionFactory = $productCollectionFactory;

        $this->catalogProductVisibility = $catalogProductVisibility;

        $this->localeDate = $localeDate;

        $this->catalogCategoryHelper = $catalogCategory;

        $this->categoryRepo = $categoryRepository;

        $this->pageHelper = $pageHelper;

        $this->categoryFactory = $categoryFactory;

        $this->catalogConfig = $catalogContext->getCatalogConfig();

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $resource,
            $resourceCollection,
            $data
        );

    }//end __construct()


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\MegaMenu\Model\ResourceModel\Menu');

    }//end _construct()


    public function getF1Children()
    {
        $f1_level   = (intval($this->getLevel()) + 1);
        $id         = $this->getId();
        $collection = $this->getResourceCollection()->addFieldToFilter('parent_id', $id)->addFieldToFilter('level', $f1_level)
            ->setOrder(
                'sort',
                \Magento\Framework\DB\Select::SQL_ASC
            );
        ;
        return $collection;

    }//end getF1Children()


    public function hasChildren()
    {
        $hasChild   = false;
        $f1_level   = (intval($this->getLevel()) + 1);
        $id         = $this->getId();
        $collection = $this->getResourceCollection();
        $collection->addFieldToFilter('parent_id', $id)->addFieldToFilter('level', $f1_level)
            ->setOrder(
                'sort',
                \Magento\Framework\DB\Select::SQL_ASC
            );
        if ($collection->getSize() > 0) {
            $hasChild = true;
        }

        return $hasChild;

    }//end hasChildren()


    public function getHtmlFrontend($tops)
    {
        // $tops;
        $html = '';
        foreach ($tops as $top) {
            $html .= $this->get_submenu_html_format($top);
        }

    }//end getHtmlFrontend()

    public function getTopLevelMenu()
    {
        $collection = $this->getResourceCollection()->addFieldToFilter('level', 0)->setOrder(
            'sort',
            \Magento\Framework\DB\Select::SQL_ASC
        );
        return $collection;

    }//end getTopLevelMenu()


    public function getMenuHtmlFormat()
    {
        $topLevelMenus = $this->getTopLevelMenu();
        // $menu =  '<ul class="nav-links">';
        $menu = '';
        foreach ($topLevelMenus as $top) {
            $menu .= $this->getHtmlMenu($top);
        }

        // $menu .="</ul>";
        $this->_logger->debug($menu);
        return  $menu;

    }//end getMenuHtmlFormat()


    public function getCategoryMenu($categories, $count)
    {
        $tree = '';

        $tree = '<ul class="submenu">';
        foreach ($categories as $categoryId) {
            if (is_object($categoryId)) {
                $categoryId = $categoryId->getId();
            }

            $cat   = $this->categoryFactory->create()->load($categoryId);
            $link  = $this->catalogCategoryHelper->getCategoryUrl($cat);
            $title = $cat->getName();
            if ($cat->hasChildren()) {
                $liClass = "parent";
            } else {
                $liClass = '';
            }

            $tree .= "<li class='{$liClass}'>";
            $tree .= "<a href='$link' >$title </a>";
            $tree .= ' <span class="ui-menu-icon"></span>';

            if ($cat->hasChildren()) {
                $count++;
                $children = $cat->getCategories($categoryId);
                $tree    .= $this->getCategoryMenu($children, $count);
            }

            $tree .= '</li>';
        }//end foreach

        return  $tree.'</ul>';

    }//end getCategoryMenu()


    public function getProductSection($productCollection, $heading)
    {
        $class = "megamenu-right col-4";

        $html    = "<li class='$class' >";
        // $heading = __('Featured Product');
        $html   .= "<h2>$heading </h2>";

        $responsiveItemFor480 = min(2, $productCollection->getSize());
        $responsiveItemFor600 = min(4, $productCollection->getSize());
        $responsiveItemFor992 = min(4, $productCollection->getSize());


        $html .= "<div class='product-items owl-carousel ' data-margin='20' data-responsive='{\"0\":{\"items\":1},\"480\":{\"items\":$responsiveItemFor480},\"600\":{\"items\":$responsiveItemFor600},\"992\":{\"items\":$responsiveItemFor992}}'  data-autoplayTimeout=\"700\" data-autoplay=\"true\" data-loop=\"true\">";

        if ($productCollection->getSize() > 0) {
            /** @var  $product \Magento\Catalog\Model\Product */
            foreach ($productCollection as $product) {
                $productName = $product->getName();
                $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
                $imageBuilder = $objectManager->create('\Magento\Catalog\Block\Product\ImageBuilder');
                $productImage     = $imageBuilder->setProduct($product)->setImageId('category_page_grid')->create();
                $productImageHtml = $productImage->toHtml();
                $price            = $product->getFormatedPrice();

                $html            .= "<div class='product-item'>
    <div class='product-item-info'>
        <a class='product-item-photo' href=''>

            {$productImageHtml}
        </a>
        <div class='product-item-details'>
            <strong class='product-item-name'>
                <a class='product-item-link' href='' title='{$productName}'>
                   {$productName}                                   </a>
            </strong>
            <div data-product-id='14' data-role='priceBox' class='price-box price-final_price'>


                                                                    <span class='price-container price-final_price tax weee'>
                                                                        <span class='price-wrapper price-including-tax' data-price-type='finalPrice' data-price-amount='48.712501' data-label='Incl. Tax' id='price-including-tax-old-price-14-widget-product-grid'>
                                                                            <span class='price'>$price</span>
                                                                        </span>
                                                                    </span>

            </div>
        </div>
    </div>
</div>";
            }//end foreach
        }//end if

        return $html;

    }//end getProductSection()


    public function getMenuArrayFormat()
    {
        $topLevelMenus = $this->getTopLevelMenu();
        $out           = $this->getMenuArray($topLevelMenus);

        // $menus = $this->flatten($out);
        $arfFormat = array();
        foreach ($out as $key => $value) {
            $menu       = $this->load($value);
            $parentId   = $menu->getData('parent_id');
            $parentName = '';


            $brother_name = $menu->getData('brother_name');

            if ($brother_name) {
                $hasBrother = true;
            } else {
                $hasBrother = false;
            }

            $arfFormat[] = array(
                'id'           => $menu->getId(),
                'title'        => $menu->getTitle(),
                'level'        => $menu->getLevel(),
                'sort'         => $menu->getSort(),
                'parent'       => $parentId,
                'hasParent'    => false,
                'parentName'   => $parentName,
                'hasBrother'   => $hasBrother,
                'brother_name' => $brother_name,
                'label_name'   => $menu->getData('label_name'),
                'label_color'  => $menu->getData('label_color'),
                'is_mega'      => $menu->getData('is_mega'),
                'type'         => $menu->getData('type'),
                'obj_id'       => $menu->getData('obj_id'),
                'link'         => $menu->getData('link'),
                'is_new'       => false,
                'megaColumn'   => $menu->getData('megaColumn'),

            );
        }//end foreach

        return $arfFormat;

    }//end getMenuArrayFormat()


    public function getChildrenRecu($menu)
    {

        $menus = $menu->getF1Children();
        if ($menus->getSize() > 0) {
            foreach ($menus as $theMenu) {
                $this->_allChildren[] = $theMenu->getId();

                if ($theMenu->hasChildren()) {
                    $this->getChildrenRecu($theMenu);
                }
            }
        }

        return $this->_allChildren;

    }//end getChildrenRecu()


    public function getMenuArray($topLevelMenus)
    {
        $out = array();
        if ($topLevelMenus->getSize() > 0) {
            foreach ($topLevelMenus as $theMenu) {
                $out[] = $theMenu->getId();
                $this->_allChildren = array();
                $chilrenIds         = $this->getChildrenRecu($theMenu);
                $chilrenIds         = ($chilrenIds);
                foreach ($chilrenIds as $c) {
                    $out[] = $c;
                }
            }
        }

        return $out;

    }//end get_menu_array()


    public function getMenuArr($topLevelMenus)
    {
        $out = array();
        if ($topLevelMenus->getSize() > 0) {
            foreach ($topLevelMenus as $theMenu) {
                $link   = $theMenu->getLink();
                $title  = $theMenu->getTitle();
                $level  = $theMenu->getLevel();
                $isMega = $theMenu->getData('is_mega');

                $out[] = $theMenu->getId();

                if ($theMenu->hasChildren()) {
                    $children = $theMenu->getF1Children();
                    $me       = $this->getMenuArr($children);
                    foreach ($children as $c) {
                        $out[] = $c;
                    }
                }
            }
        }

        return $out;

    }//end get_menu_arr()


    public function getAllChildren()
    {

        $out = $this->getF1Children();

        if ($out) {
            foreach ($out as $child) {
                $menu  = $this->load($child['id']);
                $out[] = $menu->getAllChildren();
            }
        }

        return $out;

    }//end getAllChildren()


    public function getNestedMenu()
    {
        $top = $this->getTopLevelMenu();
        foreach ($top as $key => $menu) {
            $model = $this->load($menu['id']);
            $top[$key]['children'] = $model->getF1Children();

            foreach ($top[$key]['children'] as $key => $value) {
                $top[$key]['children'][$key]['children'] = $model->load($value['id'])->getF1Children();
            }
        }

    }//end getNestedMenu()


    public function getMenuHtml()
    {
        $tops = $this->getTopLevelMenu();
        $out  = $this->getMenuHtmlR($tops, 0);

    }//end getMenuHtml()


    public function getMenuHtmlR($tops, $level = 0)
    {
        $html = "<ul>";

        foreach ($tops as $menu) {
            $level = 'item-lv'.$level;
            $class = "class ='{$level} ";
            $html .= '<li'."{$class}".'>';
            $html .= "<a > ".$menu['title']."</a>";

            $children = $this->load($menu['id'])->getF1Children();

            if ($children) {
                $level++;
                $html .= getMenuHtmlR($children, $level);
            }

            $html .= '</li>';
        }

        return  $html.'</ul>';

    }//end getMenuHtmlR()


    public function getProductCollection($category)
    {
        $todayStartOfDayDate = $this->localeDate->date()->setTime(0, 0, 0)->format('Y-m-d H:i:s');
        $todayEndOfDayDate   = $this->localeDate->date()->setTime(23, 59, 59)->format('Y-m-d H:i:s');

        /*
            * @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection
        */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());

        // condition from date
        $condFromDate = array(
            'or' => array(
                0 => array(
                    'date' => true,
                    'to'   => $todayEndOfDayDate,
                ),                                  1 => array('is' => new \Zend_Db_Expr('null'))
            ),
        );

        // condToDate
        $condToDate = array(
            'or' => array(
                0 => array(
                    'date' => true,
                    'from' => $todayStartOfDayDate,
                ),                                1 => array('is' => new \Zend_Db_Expr('null'))
            ),
        );

        $collection = $this->addProductAttributesAndPrices(
            $collection
        )->addStoreFilter()->addAttributeToFilter('news_from_date', $condFromDate, 'left')
            ->addAttributeToFilter('news_to_date', $condToDate, 'left')->addAttributeToFilter(
                array(
                    array(
                        'attribute' => 'news_from_date',
                        'is'        => new \Zend_Db_Expr('not null'),
                    ),
                    array(
                        'attribute' => 'news_to_date',
                        'is'        => new \Zend_Db_Expr('not null'),
                    ),
                )
            )->addCategoryFilter($category)
            ->setPageSize(
                $this->getProductsCount()
            )->setCurPage(
                1
            );

        return $collection;

    }//end getProductCollection()


    public function getFeaturedProduct($category)
    {
        /*
            * @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection
        */
        $collection = $this->productCollectionFactory->create();
        $collection->setVisibility($this->catalogProductVisibility->getVisibleInCatalogIds());
        $collection = $this->addProductAttributesAndPrices($collection)
            ->addAttributeToFilter('menu_featured', array('eq', '1'))->addCategoryFilter($category);
        return $collection;

    }//end getFeaturedProduct()


    protected function addProductAttributesAndPrices(
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
    ) {
        return $collection
            ->addMinimalPrice()
            ->addTaxPercents()
            ->addFinalPrice()
            ->addAttributeToSelect($this->catalogConfig->getProductAttributes())
            ->addUrlRewrite();

    }//end addProductAttributesAndPrices()

    /**
     * get Absolute Path to the Parent
     */
    public function getRPath()
    {
        $menuId =$this->getId();

        if (!$menuId) {
            return $menuId;
        }

    }

    public function getHtmlMenu($menu)
    {
        $html = '';
        $level = $menu->getLevel();
        $isMega = $menu ->getData('is_mega');
        $column = $menu->getData('megaColumn');

        $link       = $menu->getData('link');
        $title      = $menu->getData('title');
        $label_name  = $menu->getData('label_name');
        $label_color = $menu->getData('label_color');

        $type = $menu->getType();
        $obj_id = $menu->getData('obj_id');
        $parentMenu = $this->load($menu->getData('parent_id'));

        //get the icon of menu item
        $icon     = '';
        $icon_src = $menu->getData('icon');

        if ($icon_src) {
            $icon = "<img src='$icon_src' class='mega-icon'/>" ;
        }

        // get the label of the menu item
        $label       = '';
        $label_name  = $menu->getData('label_name');
        $label_color = $menu->getData('label_color');

        if ($label_name) {
            $label = "<span class='cat-label  mega-label $label_color' > $label_name </span>";
        }

        //the column
        $megaColumn = $menu->getData('megaColumn');
        if (!$megaColumn) {
            $megaColumn = 4;
        }

        //depend on the type of the menu we get the corresponding link
        if ($type == 'category') {
            /*
              * @var  $category \Magento\Catalog\Model\Category
          */
            $category = $this->categoryRepo->get($obj_id);

            $link = $this->catalogCategoryHelper->getCategoryUrl($category);
        }

        //if type is page then get the link of the page
        if ($type === 'page') {
            $link = $this->pageHelper->getPageUrl($obj_id);
        }

        if ($menu->getData('type') == 'text') {
            $title = htmlspecialchars_decode($title);
            $link  = htmlspecialchars_decode($link);
        }

        //end of the title and link correction process

        $liClass = "";
        $arrowSymbol ='';

        if ($menu->hasChildren()) {
            $liClass .= " parent item-parent";
            // if (!$isMega) $arrowSymbol ="<span class='ui-menu-icon'> </span>";
        } else {
            $liClass .=" item-no-parent";
        }

        if ($isMega == '1' && $level =='0') {
            $liClass .= " megamenu";
        }

        //if the  submenu item is level 1 and parent of it is mega menu then it has col class
        if ($level == '1' && $parentMenu->getData('is_mega') == '1') {
            $liClass .= " col-$megaColumn";
        }

        $html .="<li class='$liClass'>";
        $html .= "<a href='$link'> $icon $title  $label $arrowSymbol</a> ";


        if ($menu->hasChildren() && $type != 'category') {
            $theClass = "submenu";
            $html .= "<ul class=\"{$theClass}\">";
            $childrens = $menu->getF1Children();

            foreach ($childrens as $child) {
                $html .= $this->getHtmlMenu($child);
            }

            $html .= "</ul>";
        }

        //if type is category

        if ($type == "category") {
            //there are some cases
            if ($this->hasChildrenForCategoryItem($menu)) {
                $theClass = "submenu";
                $html .= "<ul class=\"{$theClass}\">";

                $childrens = $menu->getF1Children();

                foreach ($childrens as $child) {
                    $html .= $this->getHtmlMenu($child);
                }


                //get the children that derived from sub category

                $isCategoryHasChildren = $category->hasChildren();

                if ($isCategoryHasChildren) {
                    if ($menu->getLevel() == '0' &&  $menu->getData('is_mega') == '1') {
                        $html .= "<li class='col-8'>";
                        $html .= "<ul class='submenu'>";
                    }

                    $subCategories = $category->getCategories($obj_id, 1);

                    foreach ($subCategories as $subCat) {
                        $html .= $this->getCategoryMenuHtml($subCat, 0);
                    }

                    if ($menu->getLevel() == '0' &&  $menu->getData('is_mega') == '1') {
                        $html .= "</ul>";
                        $html .= "</li>";
                    }
                } //end of if
                //end of get the sub category

                //get the product section (best seller or new arrival)

                if ($level == '0') {
                    $includeChild = $menu->getData('include_child');

                    $productType = $menu->getData('show_product');
                    if ($includeChild === 'yes' && $isMega == '1') {
                        if ($productType === 'new_arrival') {
                            $news = $this->getProductCollection($category);

                            $productTitle = __("New Arrival");
                            $productSection = $this->getProductSection($news, $productTitle);
                            // $html          .= "<li class='megamenu-right col-4' >";

                            $html .= $productSection;
                            $html .= "</li>";
                        } elseif ($productType === 'best_seller') {
                            $featuredProductCollection = $this->getFeaturedProduct($category);
                            $productTitle = __("Featured Product");
                            $productSection = $this->getProductSection($featuredProductCollection, $productTitle);
                            // $html          .= "<li class='megamenu-right col-6' >";

                            $html .= $productSection;
                            $html .= "</li>";
                        }
                    }
                }

                $html .= "</ul>";
            }
        }

        return $html;
    }

    /**
     * @param $subCat \Magento\Catalog\Model\Category
     */
    private function getCategoryMenuHtml($cat, $count)
    {
        $tree ='';
        $cat = $this->categoryRepo->get($cat->getId());

        $link  = $this->catalogCategoryHelper->getCategoryUrl($cat);
        $title = $cat->getName();

        //todo :maybe check the parent at level 0 is mega to add the col class
        $liClass = 'level-'.$count;
        if ($cat->hasChildren()) {
            $liClass .= " parent item-parent";
        } else {
            $liClass .= " item-no-parent";
        }

        $tree .= "<li class='{$liClass}'>";
        $tree .= "<a href='$link' >$title </a>";

        if ($cat->hasChildren()) {
            $tree .= ' <span class="ui-menu-icon"></span>';
            $theClass = "submenu";
            $tree .= "<ul class=\"{$theClass}\">";
            ;
            $count++;
            $children = $cat->getCategories($cat->getId());

            if ($children) {
                foreach ($children as $child) {
                    $tree    .= $this->getCategoryMenuHtml($child, $count);
                }
            }

            $tree .= "</ul>";
        }

        $tree .= '</li>';
        return $tree;
    }
    /**
     * @param $menu
     */
    private function hasChildrenForCategoryItem($menu)
    {

        $out = false;
        $obj_id = $menu->getData('obj_id');

        /*
                       * @var  $category \Magento\Catalog\Model\Category
                   */
        $cat   = $this->categoryFactory->create()->load($obj_id);

        $isCathasChildren = $cat->hasChildren();

        if ($isCathasChildren) {
            $out= true;
        }

        if ($menu->hasChildren()) {
            $out= true;
        }

        $includeChild = $menu->getData('include_child');

        $productType = $menu->getData('show_product');

        if ($includeChild === 'yes') {
            if ($productType === 'new_arrival') {
                $news = $this->getProductCollection($cat);

                if ($news->getSize() > 0) {
                    $out = true;
                }
            } elseif ($productType === 'best_seller') {
                $featuredProductCollection = $this->getFeaturedProduct($cat);
                if ($featuredProductCollection->getSize() > 0) {
                    $out = true;
                }
            }
        }

        return $out;
    }
}//end class
