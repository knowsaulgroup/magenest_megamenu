<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 24/05/2017
 * Time: 22:10
 */

namespace Magenest\MegaMenu\Helper;

class Page extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_pageRepository;

    protected $_pageFactory;

    protected $_storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Cms\Model\PageRepository $pageRepository,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_pageRepository   = $pageRepository;
        $this->_pageFactory = $pageFactory;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }//end __construct()

    public function getPages()
    {
        $pages = $this->_pageFactory->create()->getCollection();
        $pageDatas = array();

        if ($pages->getSize() > 0) {
            foreach ($pages as $page) {
                $page_url = $this->_storeManager->getStore()->getBaseUrl().$page->getIdentifier();
                $relative = parse_url($page_url, PHP_URL_PATH);
                $pageData['title'] = $page->getTitle();
                $pageData['id'] = $page->getId();
                $pageData['page_id'] = $page->getId();
                $pageData['link'] = $relative;
                $pageDatas[] = $pageData;
            }
        }

        return $pageDatas;
    }
}
