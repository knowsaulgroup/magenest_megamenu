<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 22/04/2016
 * Time: 21:57
 */
namespace Magenest\MegaMenu\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $_menuFactory;

    protected $_menuEntityFactory;

    protected $_pageHelper;

    protected $_categoryHelper;

    protected $_coreRegistry;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magenest\MegaMenu\Model\MegaMenuFactory $menuFactory,
        \Magenest\MegaMenu\Model\MenuEntityFactory $menuEntityFactory,
        \Magenest\MegaMenu\Helper\Page $pageHelper,
        \Magento\Framework\Registry $registry,
        \Magenest\MegaMenu\Helper\Category $cagegoryHelper
    ) {
        $this->_menuFactory = $menuFactory;
        $this->_menuEntityFactory = $menuEntityFactory;

        $this->_pageHelper = $pageHelper;
        $this->_categoryHelper = $cagegoryHelper;
        $this->_coreRegistry = $registry;

        parent::__construct($context);

    }//end __construct()


    public function getMenusArrayFormat()
    {

    }//end getMenusArrayFormat()

    public function getMenuId()
    {
        $id=$this->_coreRegistry->registry('menu_id');
        return $id;
    }

    /**
     * get Configuration
     */
    public function getConfigTree()
    {
        $id=$this->_coreRegistry->registry('menu_id');

        $result = array(
            'menu_id' =>'',
            'menu_name' =>'',
            'is_active' => '',
            'menu_item' =>array(),
            'pages' =>array(),
            'cats' =>array(),
            'save_url' => $this->_getUrl('menu/menu/save')
        );

//        if (!$id) {
//            return $result;
//        }

        $menuModel = $this->_menuFactory->create()->load($id);
        $result['menu_id'] = $menuModel->getId();
        $result['menu_name'] = $menuModel->getName();
        $result['is_active'] = $menuModel->getIsActive();

        $result['menu_item'] = array();

        /** get the menu item associated with the menu */
        $menuItemCollection = $this->_menuEntityFactory->create()->getCollection()
            ->addFieldToFilter('menu_id', $id);

        if ($menuItemCollection->getSize() > 0) {
            foreach ($menuItemCollection as $menuItem) {
                $menuItem->load($menuItem->getEntityId());

                $menuItemData = $menuItem->getData();
                $menuItemData['id'] = $menuItem->getEntityId();
                $menuItemData['is_new'] = 'false';
                $result['menu_item'][] = $menuItemData;
            }
        }

        $result['pages'] = $this->_pageHelper->getPages();
        $result['cats'] = $this->_categoryHelper->getCategories();
        $result['save_url'] = $this->_getUrl('menu/menu/save');

        return $result;
    }
    public function getConfig()
    {
        $id=$this->_coreRegistry->registry('menu_id');

        $result = array(
            'menu_id' =>'',
            'menu_name' =>'',
            'is_active' => '',
            'menu_item' =>array(),
            'pages' =>array(),
            'cats' =>array(),
            'save_url' => $this->_getUrl('menu/menu/save')
        );

        if (!$id) {
            $menuModel = $this->_menuFactory->create();
        } else {
            $menuModel = $this->_menuFactory->create()->load($id);
        }

        $result['menu_id'] = $menuModel->getId();
        $result['menu_name'] = $menuModel->getName();
        $result['is_active'] = $menuModel->getIsActive();

        $result['menu_item'] = array();

        /** get the menu item associated with the menu */
        $menuItemCollection = $this->_menuEntityFactory->create()->getCollection()
            ->addFieldToFilter('menu_id', $id)->addFieldToFilter('level', '0')->setOrder(
                'sort',
                \Magento\Framework\DB\Select::SQL_ASC
            );

        if ($menuItemCollection->getSize() > 0) {
            foreach ($menuItemCollection as $menuItem) {
                $menuItem->load($menuItem->getEntityId());

                $menuItemData = $menuItem->getChildrenTreeFormat();
                $menuItemData['id'] = $menuItem->getEntityId();
                $menuItemData['is_new'] = 'false';
                $result['menu_item_nest'][] = $menuItemData;
            }
        }

        if (!$id) {
            $result['menu_item_nest'] = array();
        }

        $result['pages'] = $this->_pageHelper->getPages();
        $result['cats'] = $this->_categoryHelper->getCategories();
        $result['save_url'] = $this->_getUrl('menu/menu/save');

        return $result;
    }



    /**
     * @return array
     */
    public function getConfigMockup()
    {
        $out =

                array('pages' => array(
                        array('id' => 1, 'link' => 'http://google.com', 'title' => "Home"),

                        array('id' => 2, 'link' => 'http://bing.com', 'title' => "Contactus")
                    ),
                'cats' =>array(
                        array('id' => 1, 'link' => 'http://google.com', 'title' => "Cat1"),

                        array('id' => 2, 'link' => 'http://bing.com', 'title' => "Cat2")
                    ),

                );
        return $out;
    }
}//end class
