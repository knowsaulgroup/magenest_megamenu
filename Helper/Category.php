<?php
/**
 * Created by PhpStorm.
 * User: thuy
 * Date: 24/05/2017
 * Time: 22:25
 */

namespace Magenest\MegaMenu\Helper;

class Category extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_categoryFactory;

    protected $_categoryRepository;

    protected $_storeManager;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_categoryRepository   = $categoryRepository;
        $this->_categoryFactory      = $categoryFactory;
        $this->_storeManager = $storeManager;

        parent::__construct($context);
    }//end __construct()

    /**
     * @return array
     */
    public function getCategories()
    {
        $rootCat = $this->_storeManager->getStore()->getRootCategoryId();

        $categoryModel = $this->_categoryFactory->create();
        $categoryCollection = $categoryModel->getCategories($rootCat, 4, false, true, false);
        $categoryCollection->addNameToResult()->load();
        $catData = array();

        $store_url = basename($this->_storeManager->getStore()->getBaseUrl(), '');

        if ($categoryCollection->getSize() > 0) {
            foreach ($categoryCollection as $category) {
                $cateData = $category->getData();
                $category_url = $this->_categoryRepository->get($category->getId(), $this->_storeManager->getStore()->getId())->getUrl();
                $relative = parse_url($category_url, PHP_URL_PATH);
                $cateData['title'] = $cateData['name'];
                $cateData['cat_id'] = $cateData['entity_id'];
                $cateData['link'] = $relative;
                $cateData['url'] = $relative;
                $catData[] = $cateData;
            }
        }
        return $catData;
    }
}
