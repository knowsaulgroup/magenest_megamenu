<?php
/**
 * Created by Magenest
 * User: Luu Thanh Thuy
 * Date: 05/05/2016
 * Time: 12:57
 */
namespace Magenest\MegaMenu\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Menu implements ObserverInterface
{

    protected $_logger;

    protected $_menuFactory;


    public function __construct(
        \Magenest\MegaMenu\Model\MenuFactory $menuFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_menuFactory = $menuFactory;
        $this->_logger      = $logger;

    }//end __construct()


    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $trans = $observer->getEvent()->getData('transportObject');
        $menu  = $this->_menuFactory->create();
        $html  = $menu->getMenuHtmlFormat();
        $trans->setHtml($html);
        return $observer;

    }//end execute()
}//end class
